
'#Macro declareListOf

#Define OBJTYPE Integer


Type stwList
	
Private:
	_count As Integer
	_capacity As Integer
	
	
Public:
	Declare Property Capacity() As Integer
	Declare Property Count() As Integer
	Declare Property Item(index As Integer) ByRef As OBJTYPE
	Declare Property Item(index As Integer, value As OBJTYPE)
	
	Declare Sub Add(item As OBJTYPE)
	Declare Sub Clear_()
	Declare Function Contains(item As OBJTYPE) As Integer
	Declare Function IndexOf Overload (item As OBJTYPE) As Integer
	Declare Function IndexOf Overload (item As OBJTYPE, start As Integer) As Integer
	Declare Function IndexOf Overload (Item As OBJTYPE, start As Integer, ende As Integer) As Integer
	Declare Sub Insert(item As OBJTYPE, index As Integer)
	Declare Function LastIndexOf Overload (item As OBJTYPE) As Integer
	Declare Function LastIndexOf Overload (item As OBJTYPE, start As Integer) As Integer
	Declare Function LastIndexOf Overload (Item As OBJTYPE, start As Integer, ende As Integer) As Integer
	
	
	
	Declare Constructor ()
	Declare Constructor (initialCapacity As Integer)
	'TODO: List<T>(IEnumerable<T>)
	'Declare Constructor ()
	
	Declare Destructor()
	
End Type

'Initializes a new instance of the List<T> class that is empty and has the default initial capacity.
Constructor stwList()
	
End Constructor

'Initializes a new instance of the List<T> class that is empty and has the specified initial capacity.
Constructor stwList(initialCapacity As Integer)
	
End Constructor

Destructor stwList()
	
End Destructor


'Gets or sets the total number of elements the internal data structure can hold without resizing.
'TODO: setter
Property stwList.Capacity() As Integer
	Return this._capacity
End Property

'Gets the number of elements contained in the List<T>.
Property stwList.Count() As Integer
	Return this._count
End Property

'Gets or sets the element at the specified index.
Property stwList.Item(index As Integer) ByRef As OBJTYPE

End Property
Property stwList.Item(index As Integer, value As OBJTYPE)

End Property

'Adds an object to the end of the List<T>.
Sub stwList.Add(item As OBJTYPE)
	
End Sub

'Removes all elements from the List<T>.
Sub stwList.Clear_()
	
End Sub

'Determines whether an element is in the List<T>.
Function stwList.Contains(item As OBJTYPE) As Integer
	
End Function


'Searches for the specified object and returns the zero-based index of the first occurrence within the entire List<T>.
Function stwList.IndexOf(item As OBJTYPE) As Integer
	
End Function

'Searches for the specified object and returns the zero-based index of the first occurrence within the range of elements in the List<T> that extends from the specified index to the last element.
Function stwList.IndexOf(item As OBJTYPE, start As Integer) As Integer
	
End Function

'Searches for the specified object and returns the zero-based index of the first occurrence within the range of elements in the List<T> that starts at the specified index and contains the specified number of elements.
Function stwList.IndexOf(item As OBJTYPE, start As Integer, ende As Integer) As Integer
	
End Function

'Inserts an element into the List<T> at the specified index.
Sub stwList.Insert(item As OBJTYPE, index As Integer)
	
End Sub


'Searches for the specified object and returns the zero-based index of the last occurrence within the entire List<T>.
Function stwList.LastIndexOf(item As OBTYPE) As Integer
	
End Function

'Searches for the specified object and returns the zero-based index of the last occurrence within the range of elements in the List<T> that extends from the first element to the specified index.
'Searches for the specified object and returns the zero-based index of the last occurrence within the range of elements in the List<T> that contains the specified number of elements and ends at the specified index.

'#EndMacro




'declareListOf