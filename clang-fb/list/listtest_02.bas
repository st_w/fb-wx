Type myType
	a As Integer
	b As Integer
	c As Integer
	'Declare Operator Let(a As myType)
	Declare Operator Cast() As String
End Type

/'
Operator myType.Let (a As myType)
	this.a = a.a
	this.b = a.b
	this.c = a.c
End Operator
'/

Operator = (a As myType, b As myType) As Integer
	Return (a.a = b.a) AndAlso (a.b = b.b) AndAlso (a.c = b.c)
End Operator

Operator myType.Cast() As String
	Return ""
End Operator


#Define OBJTYPE myType

#Ifndef stwListMTP

'ByRef return values supported in fbc >= 0.90.0
#If __FB_MIN_VERSION__(0,90,0)
#Define BYREFRETURN ByRef
#Else
'Return ByVal instead for older versions
#Define BYREFRETURN 
#EndIf

Type stwListMTP
	
Private:
	_count As Integer
	_capacity As Integer
	_data As UByte Ptr
#Ifdef THREADSAFE
	_mutex As Any Ptr
#endif
	
Public:
	Declare Property Capacity() As Integer
	Declare Property Count() As Integer
	Declare Property Item(index As Integer) BYREFRETURN As OBJTYPE
	Declare Property Item(index As Integer, value As OBJTYPE)
	
	Declare Sub Add(value As OBJTYPE)
	Declare Sub AddRange(list As stwListMTP)
	Declare Sub Clear_()
	Declare Function Contains(value As OBJTYPE) As Integer
	Declare Function IndexOf Overload (value As OBJTYPE) As Integer
	Declare Function IndexOf Overload (value As OBJTYPE, start As Integer) As Integer
	Declare Function IndexOf Overload (value As OBJTYPE, start As Integer, ende As Integer) As Integer
	Declare Sub Insert(value As OBJTYPE, index As Integer)
	Declare Function LastIndexOf Overload (value As OBJTYPE) As Integer
	Declare Function LastIndexOf Overload (value As OBJTYPE, start As Integer) As Integer
	Declare Function LastIndexOf Overload (value As OBJTYPE, start As Integer, ende As Integer) As Integer
	Declare Sub Remove (value As OBJTYPE)
	Declare Sub RemoveAt (index As Integer)
	Declare Sub RemoveRange (index As Integer, cnt As Integer)
	Declare Sub Reverse Overload ()
	Declare Sub Reverse Overload (start As Integer, ende As Integer)
	Declare Sub Sort Overload ()
	Declare Function ToString() As String
	Declare Sub TrimExcess()
	
	Declare Constructor ()
	Declare Constructor (initialCapacity As Integer)
	'TODO: List<T>(IEnumerable<T>)
	'Declare Constructor ()
	
	Declare Destructor()
	
	#If __FB_MIN_VERSION__(0,91,0)
	Declare Operator [] (index As Integer) ByRef As OBJTYPE
	#EndIf
	
End Type

'Initializes a new instance of the List<T> class that is empty and has the default initial capacity.
Constructor stwListMTP()
	this._data = Allocate(16 * SizeOf(OBJTYPE))
	this._capacity = 16#
	this._count = 0
#Ifdef THREADSAFE
	this._mutex = MutexCreate()
#endif
End Constructor

'Initializes a new instance of the List<T> class that is empty and has the specified initial capacity.
Constructor stwListMTP(initialCapacity As Integer)
	this._data = Allocate(initialCapacity * SizeOf(OBJTYPE))
	this._capacity = initialCapacity
	this._count = 0
#Ifdef THREADSAFE
	this._mutex = MutexCreate()
#EndIf
End Constructor

Destructor stwListMTP()
	DeAllocate(this._data)
#Ifdef THREADSAFE
	MutexDestroy(this._mutex)
#EndIf
End Destructor


'Gets or sets the total number of elements the internal data structure can hold without resizing.
'TODO: setter
Property stwListMTP.Capacity() As Integer
	Return this._capacity
End Property

'Gets the number of elements contained in the List<T>.
Property stwListMTP.Count() As Integer
	Return this._count
End Property


'Gets or sets the element at the specified index.
Property stwListMTP.Item(index As Integer) BYREFRETURN As OBJTYPE
'FIXME
	'If index < 0 Or index >= this._count Then Return 0
	Return *Cast(OBJTYPE ptr, this._data + index * SizeOf(OBJTYPE))
End Property

Property stwListMTP.Item(index As Integer, value As OBJTYPE)
	If index < 0 Or index >= this._count Then Return
	*Cast(OBJTYPE ptr, this._data + index * SizeOf(OBJTYPE)) = value
	Print "### called ###"
End Property


'Adds an object to the end of the List<T>.
Sub stwListMTP.Add(value As OBJTYPE)
	If this._count+1 < this._capacity Then
		this._capacity += Int(this._capacity / 2)
		this._data = ReAllocate(this._data, This._capacity * SizeOf(OBJTYPE))
	EndIf
	*Cast(OBJTYPE ptr, this._data + this._count * SizeOf(OBJTYPE)) = value
	this._count += 1
End Sub

'Removes all elements from the List<T>.
Sub stwListMTP.Clear_()
	this._count = 0
End Sub

'Determines whether an element is in the List<T>.
Function stwListMTP.Contains(value As OBJTYPE) As Integer
	For q As Integer = 0 To this._count-1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return -1
	Next
	Return 0
End Function

'Searches for the specified object and returns the zero-based index of the first occurrence within the entire List<T>.
Function stwListMTP.IndexOf(value As OBJTYPE) As Integer
	For q As Integer = 0 To this._count-1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Searches for the specified object and returns the zero-based index of the first occurrence within the range of elements in the List<T> that extends from the specified index to the last element.
Function stwListMTP.IndexOf(value As OBJTYPE, start As Integer) As Integer
	For q As Integer = start To this._count-1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Searches for the specified object and returns the zero-based index of the first occurrence within the range of elements in the List<T> that starts at the specified index and contains the specified number of elements.
Function stwListMTP.IndexOf(value As OBJTYPE, start As Integer, ende As Integer) As Integer
	If start < 0 OrElse ende >= this._count Then Return -1
	For q As Integer = start To ende
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Inserts an element into the List<T> at the specified index.
Sub stwListMTP.Insert(value As OBJTYPE, index As Integer)
	If index < 0 Or index > this._count Then Return
	If this._count+1 < this._capacity Then
		this._capacity += Int(this._capacity / 2)
		this._data = ReAllocate(this._data, This._capacity * SizeOf(OBJTYPE))
	EndIf
	this._count += 1
	For q As Integer = this._count-1 To index+1 Step -1
		*Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = *Cast(OBJTYPE ptr, this._data + (q-1) * SizeOf(OBJTYPE))
	Next
	*Cast(OBJTYPE ptr, this._data + index * SizeOf(OBJTYPE)) = value
End Sub

'Searches for the specified object and returns the zero-based index of the last occurrence within the entire List<T>.
Function stwListMTP.LastIndexOf(value As OBJTYPE) As Integer
	For q As Integer = this._count-1 To 0 Step -1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Searches for the specified object and returns the zero-based index of the last occurrence within the range of elements in the List<T> that extends from the first element to the specified index.
Function stwListMTP.LastIndexOf(value As OBJTYPE, start As Integer) As Integer
	For q As Integer = this._count-1 To start Step -1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Searches for the specified object and returns the zero-based index of the last occurrence within the range of elements in the List<T> that contains the specified number of elements and ends at the specified index.
Function stwListMTP.LastIndexOf(value As OBJTYPE, start As Integer, ende As Integer) As Integer
	For q As Integer = IIf(this._count-1<ende, This._count-1, ende) To start Step -1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Removes the first occurrence of a specific object from the List<T>.
Sub stwListMTP.Remove (value As OBJTYPE)
	this.RemoveAt(this.IndexOf(value))
End Sub

'Removes the element at the specified index of the List<T>.
Sub stwListMTP.RemoveAt (index As Integer)
	If index < 0 Or index >= this._count Then Return
	For q As Integer = index To this._count-2
		*Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = *Cast(OBJTYPE ptr, this._data + (q+1) * SizeOf(OBJTYPE))
	Next
	this._count-=1
End Sub

'Removes a range of elements from the List<T>.
Sub stwListMTP.RemoveRange (index As Integer, cnt As Integer)
	If index < 0 Or cnt < 1 Or index+cnt > this._count Then Return
	For q As Integer = index To this._count-1-cnt
		*Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = *Cast(OBJTYPE ptr, this._data + (q+cnt) * SizeOf(OBJTYPE))
	Next
	this._count-=cnt
End Sub

'Reverses the order of the elements in the entire List<T>.
Sub stwListMTP.Reverse ()
	For q As Integer = 0 To Int(This._count/2)-1
		Swap *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)), *Cast(OBJTYPE ptr, this._data + (this._count-1-q) * SizeOf(OBJTYPE))
	Next
End Sub

'Reverses the order of the elements in the entire List<T>.
Sub stwListMTP.Reverse (start As Integer, ende As Integer)
	For q As Integer = 0 To Int((ende-start+1)/2)-1
		Swap *Cast(OBJTYPE ptr, this._data + (start+q) * SizeOf(OBJTYPE)), *Cast(OBJTYPE ptr, this._data + (ende-q) * SizeOf(OBJTYPE))
	Next
End Sub

'Sorts the elements in the entire List<T> using the default comparer.
Sub stwListMTP.Sort ()
	
End Sub

'Returns a string that represents the current object.
Function stwListMTP.ToString() As String
	Dim As String s = ""
	If this._count = 0 Then Print "<empty>"
	For q As Integer = 0 To this._count-1
		's += Str(*Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)))
		#Print TypeOf((*Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE))))
		s += Cast(String, (*Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE))))
		If q < this._count-1 Then s += ", "
	Next
	Return s
End Function


'Sets the capacity to the actual number of elements in the List<T>, if that number is less than a threshold value.
Sub stwListMTP.TrimExcess()
	If this._count > this._capacity*0.9 Then Return
	If this._count = 0 Then
		DeAllocate(this._data)
		this._data = Allocate(16 * SizeOf(OBJTYPE))
	Else
		this._data = ReAllocate(this._data, (This._count + Int(This._count/2)) * SizeOf(OBJTYPE))
	EndIf
End Sub

#If __FB_MIN_VERSION__(0,91,0)

Operator stwListMTP.[] (index As Integer) ByRef As OBJTYPE
	Return this.Item(index)
End Operator

#EndIf

#EndIf







'declareList(myType, MTP)

Dim As stwListMTP l3


l3.Add( Type<myType> (1,2,3) )




Dim a As Integer
Dim s As String


'Operator Cast(a As Integer) As String
'	Return Str(a)
'End Operator


#Print TypeOf(Cast(String, a))

s = Cast(String, a)













