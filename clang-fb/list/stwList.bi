
#Macro declareList(OBJTYPE, LISTNAME)

#Ifndef stwList##LISTNAME

#Include Once "crt.bi"

'ByRef return values supported in fbc >= 0.90.0
#If __FB_MIN_VERSION__(0,90,0)
#Define BYREFRETURN ByRef
#Else
'Return ByVal instead for older versions

#Define BYREFRETURN 
#EndIf

Type stwList##LISTNAME
	
Private:
	_count As Integer
	_capacity As Integer
	_data As UByte Ptr
#Ifdef THREADSAFE
	_mutex As Any Ptr
#EndIf
	
Public:
	Declare Property Capacity() As Integer
	Declare Property Count() As Integer
	Declare Property Item(index As Integer) BYREFRETURN As OBJTYPE
	Declare Property Item(index As Integer, value As OBJTYPE)
	
	Declare Sub Add(value As OBJTYPE)
	Declare Sub AddRange(list As stwList##LISTNAME)
	Declare Sub Clear_()
	Declare Function Contains(value As OBJTYPE) As Integer
	Declare Function IndexOf Overload (value As OBJTYPE) As Integer
	Declare Function IndexOf Overload (value As OBJTYPE, start As Integer) As Integer
	Declare Function IndexOf Overload (value As OBJTYPE, start As Integer, ende As Integer) As Integer
	Declare Sub Insert(value As OBJTYPE, index As Integer)
	Declare Function LastIndexOf Overload (value As OBJTYPE) As Integer
	Declare Function LastIndexOf Overload (value As OBJTYPE, start As Integer) As Integer
	Declare Function LastIndexOf Overload (value As OBJTYPE, start As Integer, ende As Integer) As Integer
	Declare Sub Remove (value As OBJTYPE)
	Declare Sub RemoveAll (value As OBJTYPE)
	Declare Sub RemoveAt (index As Integer)
	Declare Sub RemoveRange (index As Integer, cnt As Integer)
	Declare Sub Reverse Overload ()
	Declare Sub Reverse Overload (start As Integer, ende As Integer)
	Declare Sub Sort Overload ()
	Declare Function ToString() As String
	Declare Sub TrimExcess()
	
	Declare Constructor ()
	Declare Constructor (initialCapacity As Integer)
	Declare Constructor (ByRef list As stwList##LISTNAME)
	'TODO: List<T>(IEnumerable<T>)
	'Declare Constructor ()
	
	Declare Destructor()
	
	Declare Operator Let(list As stwList##LISTNAME)
	
	#If __FB_MIN_VERSION__(0,91,0)
	Declare Operator [] (index As Integer) ByRef As OBJTYPE
	#EndIf
	
End Type

#EndIf

#EndMacro


#Macro implementList(OBJTYPE, LISTNAME, CALL_DESTRUCTOR)

'Initializes a new instance of the List<T> class that is empty and has the default initial capacity.
Constructor stwList##LISTNAME()
	this._data = Callocate(16 * SizeOf(OBJTYPE))
	#If __FB_DEBUG__
		If this._data = 0 Then Print "ERROR: list.Constructor() cannot allocate memory"
	#EndIf
	this._capacity = 16
	this._count = 0
#Ifdef THREADSAFE
	this._mutex = MutexCreate()
#endif
End Constructor

'Initializes a new instance of the List<T> class that is empty and has the specified initial capacity.
Constructor stwList##LISTNAME(initialCapacity As Integer)
	this._data = Callocate(initialCapacity * SizeOf(OBJTYPE))
	#If __FB_DEBUG__
		If this._data = 0 Then Print "ERROR: list.Constructor() cannot allocate memory"
	#EndIf
	this._capacity = initialCapacity
	this._count = 0
#Ifdef THREADSAFE
	this._mutex = MutexCreate()
#EndIf
End Constructor

'Copy Constructor
Constructor stwList##LISTNAME(ByRef list As stwList##LISTNAME)
	#If __FB_DEBUG__
		'Print "stwList: COPY CONSTRUCTOR was called"
	#EndIf
	this._data = Callocate(list._capacity * SizeOf(OBJTYPE))
	#If __FB_DEBUG__
		If this._data = 0 Then Print "ERROR: list.Constructor() cannot allocate memory"
	#EndIf
	memcpy(this._data, list._data, list._capacity * SizeOf(OBJTYPE))
	this._capacity = list._capacity
	this._count = list._count
#Ifdef THREADSAFE
	this._mutex = MutexCreate()
#EndIf
End Constructor


Destructor stwList##LISTNAME()
	#If TypeOf(OBJTYPE) = "STRING"
	this.Clear_()
	#ElseIf CALL_DESTRUCTOR
	this.Clear_()
	#EndIf
	DeAllocate(this._data)
#Ifdef THREADSAFE
	MutexDestroy(this._mutex)
#EndIf
End Destructor


'Gets or sets the total number of elements the internal data structure can hold without resizing.
'TODO: setter
Property stwList##LISTNAME.Capacity() As Integer
	Return this._capacity
End Property

'Gets the number of elements contained in the List<T>.
Property stwList##LISTNAME.Count() As Integer
	Return this._count
End Property


'Gets or sets the element at the specified index.
Property stwList##LISTNAME.Item(index As Integer) BYREFRETURN As OBJTYPE
	#If __FB_DEBUG__
		'Print "INFO: Accessing element at index "; index; " value: "; str(*Cast(OBJTYPE ptr, this._data + index * SizeOf(OBJTYPE)))
		If index < 0 Or index >= this._count Then Print "ERROR: list.Item(index) read access out of bounds: index "; index; ", count: ", this._count 
	#EndIf
'FIXME
	'If index < 0 Or index >= this._count Then Return 0
	Return *Cast(OBJTYPE ptr, this._data + index * SizeOf(OBJTYPE))
End Property

Property stwList##LISTNAME.Item(index As Integer, value As OBJTYPE)
	#If __FB_DEBUG__
		If index < 0 Or index >= this._count Then Print "ERROR: list.Item(index) write access out of bounds: index "; index; ", count: ", this._count 
	#EndIf
	If index < 0 Or index >= this._count Then Return
	*Cast(OBJTYPE ptr, this._data + index * SizeOf(OBJTYPE)) = value
End Property


'Adds an object to the end of the List<T>.
Sub stwList##LISTNAME.Add(value As OBJTYPE)
	If this._count >= this._capacity Then
		Dim As Integer newCapacity = this._capacity + Int(this._capacity / 2)
		'Print "INFO: Reallocating data array; old-cap: "; this._capacity; ", new-cap: "; newCapacity
		this._data = ReAllocate(this._data, newCapacity * SizeOf(OBJTYPE))
		memset(this._data + this._capacity * SizeOf(OBJTYPE), 0, (newCapacity-this._capacity)* SizeOf(OBJTYPE)) 		'FIXME _data = 0
		this._capacity = newCapacity
	EndIf
	#If __FB_DEBUG__
		'Print "INFO: Adding element at index "; this._count
		If this._data = 0 Then Print "ERROR: list.Add(value) cannot allocate memory: capacity: "; this._capacity
	#EndIf
	If this._data = 0 Then Return 'FIXME
	#If __FB_DEBUG__
		For q As Integer = 0 To SizeOf(OBJTYPE)-1
			If *(this._data + this._count * SizeOf(OBJTYPE) + q) <> 0 Then Print "WARNING: Add(value): accessing uninitialized memory, count: "; this._count
		Next
	#EndIf
	*Cast(OBJTYPE ptr, this._data + this._count * SizeOf(OBJTYPE)) = value
	this._count += 1
End Sub

'Removes all elements from the List<T>.
Sub stwList##LISTNAME.Clear_()
	#If TypeOf(OBJTYPE) = "STRING"
	For q As Integer = 0 To this._count-1
		fb_StrDelete(*Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)))
	Next
	#Else
	#If CALL_DESTRUCTOR
		For q As Integer = 0 To this._count-1
			(*Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE))).Destructor()
		Next
	#EndIf
	memset(this._data, 0, this._count * SizeOf(OBJTYPE))
	#EndIf
	this._count = 0
End Sub

'Determines whether an element is in the List<T>.
Function stwList##LISTNAME.Contains(value As OBJTYPE) As Integer
	For q As Integer = 0 To this._count-1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return -1
	Next
	Return 0
End Function

'Searches for the specified object and returns the zero-based index of the first occurrence within the entire List<T>.
Function stwList##LISTNAME.IndexOf(value As OBJTYPE) As Integer
	For q As Integer = 0 To this._count-1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Searches for the specified object and returns the zero-based index of the first occurrence within the range of elements in the List<T> that extends from the specified index to the last element.
Function stwList##LISTNAME.IndexOf(value As OBJTYPE, start As Integer) As Integer
	For q As Integer = start To this._count-1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Searches for the specified object and returns the zero-based index of the first occurrence within the range of elements in the List<T> that starts at the specified index and contains the specified number of elements.
Function stwList##LISTNAME.IndexOf(value As OBJTYPE, start As Integer, ende As Integer) As Integer
	If start < 0 OrElse ende >= this._count Then Return -1
	For q As Integer = start To ende
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Inserts an element into the List<T> at the specified index.
Sub stwList##LISTNAME.Insert(value As OBJTYPE, index As Integer)
	#If __FB_DEBUG__
		'Print "Inserting item at index "; index; " (capacity: "; this._capacity; ")"
		If index < 0 Or index > this._count Then Print "ERROR: list.Insert(value, index) write access out of bounds: index "; index; ", count: ", this._count 
	#EndIf
	If index < 0 Or index > this._count Then Return
	If this._count+1 > this._capacity Then
		Dim As Integer newCapacity = this._capacity + Int(this._capacity / 2)
		this._data = ReAllocate(this._data, newCapacity * SizeOf(OBJTYPE))
		memset(this._data + this._capacity * SizeOf(OBJTYPE), 0, newCapacity-this._capacity) 		'FIXME _data = 0
		this._capacity = newCapacity
		#If __FB_DEBUG__
			If this._data = 0 Then Print "ERROR: list.Insert(value) cannot allocate memory: capacity: "; this._capacity
		#EndIf
		If this._data = 0 Then Return 'FIXME
	EndIf
	memmove(this._data + (index+1) * SizeOf(OBJTYPE), this._data + index * SizeOf(OBJTYPE), (this._count-index) * SizeOf(OBJTYPE))
	memset(this._data + index * SizeOf(OBJTYPE), 0, SizeOf(OBJTYPE))
	*Cast(OBJTYPE ptr, this._data + index * SizeOf(OBJTYPE)) = value
	this._count += 1
End Sub

'Searches for the specified object and returns the zero-based index of the last occurrence within the entire List<T>.
Function stwList##LISTNAME.LastIndexOf(value As OBJTYPE) As Integer
	For q As Integer = this._count-1 To 0 Step -1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Searches for the specified object and returns the zero-based index of the last occurrence within the range of elements in the List<T> that extends from the first element to the specified index.
Function stwList##LISTNAME.LastIndexOf(value As OBJTYPE, start As Integer) As Integer
	For q As Integer = this._count-1 To start Step -1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Searches for the specified object and returns the zero-based index of the last occurrence within the range of elements in the List<T> that contains the specified number of elements and ends at the specified index.
Function stwList##LISTNAME.LastIndexOf(value As OBJTYPE, start As Integer, ende As Integer) As Integer
	For q As Integer = IIf(this._count-1<ende, this._count-1, ende) To start Step -1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then Return q
	Next
	Return -1
End Function

'Removes the first occurrence of a specific object from the List<T>.
Sub stwList##LISTNAME.Remove (value As OBJTYPE)
	RemoveAt(IndexOf(value))
End Sub

'Removes all the elements that match the conditions defined by the specified predicate.
Sub stwList##LISTNAME.RemoveAll (value As OBJTYPE)
	'TODO: implement this more efficiently
	For q As Integer = this._count-1 To 0 Step -1
		If *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)) = value Then RemoveAt(q)
	Next
End Sub

'Removes the element at the specified index of the List<T>.
Sub stwList##LISTNAME.RemoveAt (index As Integer)
	#If __FB_DEBUG__
		If index < 0 Or index >= this._count Then Print "ERROR: list.RemoveAt(index) write access out of bounds: index "; index; ", count: ", this._count 
	#EndIf
	If index < 0 Or index >= this._count Then Return
	#If TypeOf(OBJTYPE) = "STRING"
	'also initializes string descriptor to 0
	fb_StrDelete(*Cast(OBJTYPE ptr, this._data + index * SizeOf(OBJTYPE)))
	#Else
		#If CALL_DESTRUCTOR
		(*Cast(OBJTYPE ptr, this._data + index * SizeOf(OBJTYPE))).Destructor()
		#EndIf
	#EndIf
	memmove(this._data + index * SizeOf(OBJTYPE), this._data + (index+1) * SizeOf(OBJTYPE), (this._count-index-1) * SizeOf(OBJTYPE))
	this._count-=1
	memset(this._data + this._count * SizeOf(OBJTYPE), 0, SizeOf(OBJTYPE))	'set now unused element after last element to 0
End Sub

'Removes a range of elements from the List<T>.
Sub stwList##LISTNAME.RemoveRange (index As Integer, cnt As Integer)
	#If __FB_DEBUG__
		If index < 0 Or cnt < 1 Or index+cnt > this._count Then Print "ERROR: list.RemoveRange(index, cnt) write access out of bounds: index "; index; ", count: "; this._count 
	#EndIf
	If index < 0 Or cnt < 1 Or index+cnt > this._count Then Return
	#If TypeOf(OBJTYPE) = "STRING"
	For q As Integer = index To index+cnt-1
		'also initializes string descriptor to 0
		fb_StrDelete(*Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)))
	Next
	#Else
		#If CALL_DESTRUCTOR
		For q As Integer = index To index+cnt-1
			(*Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE))).Destructor()
		Next
		#EndIf
	#EndIf
	memmove(this._data + index * SizeOf(OBJTYPE), this._data + (index+cnt) * SizeOf(OBJTYPE), (this._count-index-cnt) * SizeOf(OBJTYPE))
	this._count-=cnt
	memset(this._data + this._count * SizeOf(OBJTYPE), 0, cnt * SizeOf(OBJTYPE))	'set now unused elements after last element to 0
End Sub

'Reverses the order of the elements in the entire List<T>.
Sub stwList##LISTNAME.Reverse ()
	For q As Integer = 0 To Int(this._count/2)-1
		Swap *Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)), *Cast(OBJTYPE ptr, this._data + (this._count-1-q) * SizeOf(OBJTYPE))
	Next
End Sub

'Reverses the order of the elements in the entire List<T>.
Sub stwList##LISTNAME.Reverse (start As Integer, ende As Integer)
	For q As Integer = 0 To Int((ende-start+1)/2)-1
		Swap *Cast(OBJTYPE ptr, this._data + (start+q) * SizeOf(OBJTYPE)), *Cast(OBJTYPE ptr, this._data + (ende-q) * SizeOf(OBJTYPE))
	Next
End Sub

'Sorts the elements in the entire List<T> using the default comparer.
Sub stwList##LISTNAME.Sort ()
	
End Sub

'Returns a string that represents the current object.
Function stwList##LISTNAME.ToString() As String
	Dim As String s = ""
	If this._count = 0 Then Print "<empty>"
	For q As Integer = 0 To this._count-1
		s += Str(*Cast(OBJTYPE ptr, this._data + q * SizeOf(OBJTYPE)))
		If q < this._count-1 Then s += ", "
	Next
	Return s
End Function


'Sets the capacity to the actual number of elements in the List<T>, if that number is less than a threshold value.
Sub stwList##LISTNAME.TrimExcess()
	If this._count > this._capacity*0.9 Then Return
	If this._count = 0 Then
		DeAllocate(this._data)
		this._data = Callocate(16 * SizeOf(OBJTYPE))
		this._capacity = 16
	Else
		Dim As Integer newCapacity = this._count + Int(this._count/2)
		If newCapacity < 16 Then newCapacity = 16
		If this._capacity <= newCapacity Then Return
		this._data = ReAllocate(this._data, newCapacity * SizeOf(OBJTYPE))
		this._capacity = newCapacity
	EndIf
	#If __FB_DEBUG__
		If this._data = 0 Then Print "ERROR: list.TrimExcess() cannot allocate memory: capacity: "; this._capacity
	#EndIf
	'If this._data = 0 Then 'FIXME
End Sub

Operator stwList##LISTNAME.Let(list As stwList##LISTNAME)
	#If __FB_DEBUG__
		'Print "stwList: LET OPERATOR was called"
	#EndIf
	this._data = Callocate(list._capacity * SizeOf(OBJTYPE))
	#If __FB_DEBUG__
		If this._data = 0 Then Print "ERROR: list.Constructor() cannot allocate memory"
	#EndIf
	memcpy(this._data, list._data, list._capacity * SizeOf(OBJTYPE))
	this._capacity = list._capacity
	this._count = list._count
	#Ifdef THREADSAFE
		this._mutex = MutexCreate()
	#EndIf
End Operator


#If __FB_MIN_VERSION__(0,91,0)

Operator stwList##LISTNAME.[] (index As Integer) ByRef As OBJTYPE
	Return this.Item(index)
End Operator

#EndIf

#EndMacro




#Macro defineList(OBJTYPE, LISTNAME, CALL_DESTRUCTOR)

declareList(OBJTYPE, LISTNAME)
implementList(OBJTYPE, LISTNAME, CALL_DESTRUCTOR)

#EndMacro
