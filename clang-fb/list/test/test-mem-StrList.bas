' String list test (memory consumption, corruption) using complete header file

#Include "../stwList.bi"



defineList(String, Str)


Dim Shared l As stwListStr


l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")

Sub test()
	
	l.Add("sub")
	Dim s As String
	s = "function"
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
End Sub

test

For q As Integer = 0 To l.Count-1
	Print l.Item(q)
	Print l[q]
Next

'change value in list (byref)
l[0] = "Hallo"
Print "1: "; l[0]


'do not change value in list via String variable
Dim a As String = "sdfsf"
a = l[0]
a = "asdfsadfsaf"
Print "2: "; l[0]


'''''''''''''''''''
Print "test memory consumption of fill, clear"

Dim l2 As stwListStr

For q As Integer = 0 To 300000
	l2.Add("abcdefghijklmnopqrstuvwxyz")
Next

l2.Clear_()

'' ensures that array is set to 0
'' needs _data to be public (in stwList)
'For q As Integer = 0 To 300000
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[0] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[1] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[2] <> 0 Then Print q
'Next


l2.TrimExcess()

Print l2.Count



'''''''''''''''''''
Print "test memory consumption of fill, remove (single)"

For q As Integer = 0 To 13000
	l2.Add("abcdefghijklmnopqrstuvwxyz")
	'Print *Cast(UInteger Ptr, @l2[q])			' allocated string data addresses
Next

Print "removing .."
For q As Integer = 0 To 13000
'	If q Mod 10000 = 0 Then Print q
	l2.RemoveAt(0)
Next

Print l2.Count

'' ensures that array is set to 0
'' needs _data to be public (in stwList)
'For q As Integer = 0 To 13000
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[0] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[1] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[2] <> 0 Then Print q
'Next

Print "trimming .."
l2.TrimExcess()



'''''''''''''''''''
Print "test memory consumption of fill, remove (range)"

For q As Integer = 1 To 10000
	l2.Add("abcdefghijklmnopqrstuvwxyz")
	'Print *Cast(UInteger Ptr, @l2[q])			' allocated string data addresses
Next

Print "removing .."
For q As Integer = 1 To 1000
'	If q Mod 10000 = 0 Then Print q
	l2.RemoveRange(0, 10)
Next

Print l2.Count

'' ensures that array is set to 0
'' needs _data to be public (in stwList)
'For q As Integer = 0 To 9999
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[0] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[1] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[2] <> 0 Then Print q
'Next

Print "trimming .."
l2.TrimExcess()

Print l2.Count


'''''''''''''''''''
Print "test memory consumption of insert, remove (by value)"

For q As Integer = 1 To 10000
	l2.Insert("abcdefghijklmnopqrstuvwxyz", 0)
	'Print *Cast(UInteger Ptr, @l2[q])			' allocated string data addresses
Next

Print l2.Count

Print "removing .."
l2.RemoveAll("abcdefghijklmnopqrstuvwxyz")

Print l2.Count

'' ensures that array is set to 0
'' needs _data to be public (in stwList)
'For q As Integer = 0 To 9999
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[0] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[1] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[2] <> 0 Then Print q
'Next

Print "trimming .."
l2.TrimExcess()

Print l2.Count

Sleep










