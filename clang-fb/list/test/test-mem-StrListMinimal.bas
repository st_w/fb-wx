' String list test (memory corruption) using minimal list implementation

Type tString
	addr As Integer
	sz As Integer
	alloc As Integer
	Declare Operator Cast() As String
End Type

Operator tString.Cast() As String
	Return Str(@this) + " (addr: " + Str(addr) + ", sz: " + Str(sz) + ", alloc: " + Str(alloc) + ")"
End Operator


'#Include "crt/string.bi"


Sub memNull naked Cdecl (addr As any Ptr, sz As UInteger)
	' stack layout:
	' [esp+12] sz
	' [esp+ 8] addr
	' [esp+ 4] RETURN
	' [esp+ 0] edi backup
	Asm
		push    edi             ' proc uses edi, so save it.
		
		Xor     eax, eax        ' store zeros (only al used)
		mov     ecx, [esp + 12] ' sz As UInteger
		mov     edi, [esp + 8]  ' addr As Any Ptr
		rep     stosb 
		
		'mov     eax, [esp + 4]  ' return pointer
		pop     edi             ' restore edi
		ret                     ' let caller adjust stack
	End Asm
End Sub





'###############################################
'###            Simple List Impl             ###
'###############################################



Type stwListStr
	
Private:
	_count As Integer
	_capacity As Integer
	_data As UByte Ptr
	
Public:
	Declare Property Count() As Integer
	Declare Property Item(index As Integer) ByRef As String
	
	Declare Sub Add(ByVal value As String)
	
	Declare Constructor ()
	Declare Destructor()
	
	Declare Operator [] (index As Integer) ByRef As String
	
End Type


'Initializes a new instance of the List<T> class that is empty and has the default initial capacity.
Constructor stwListStr()
	this._data = Allocate(16 * SizeOf(String))
	memNull(this._data, 16 * SizeOf(String))
	#If __FB_DEBUG__
		If this._data = 0 Then Print "ERROR: list.Constructor() cannot allocate memory"
	#EndIf
	this._capacity = 16
	this._count = 0
End Constructor

Destructor stwListStr()
	DeAllocate(this._data)
End Destructor

'Gets the number of elements contained in the List<T>.
Property stwListStr.Count() As Integer
	Return this._count
End Property


'Gets or sets the element at the specified index.
Property stwListStr.Item(index As Integer) byref As String
	#If __FB_DEBUG__
		Print "INFO: Accessing element at index "; index
		If index < 0 Or index >= this._count Then Print "ERROR: list.Item(index) read access out of bounds: index "; index; ", count: ", this._count 
	#EndIf
'FIXME
	'If index < 0 Or index >= this._count Then Return 0
	Return *Cast(String ptr, this._data + index * SizeOf(String))
End Property

'Adds an object to the end of the List<T>.
Sub stwListStr.Add(ByVal value As String)
	If this._count >= this._capacity Then
		Print "INFO: Reallocating data array; old-cap: "; this._capacity; ", new-cap: "; this._capacity+Int(this._capacity / 2)
		this._capacity += Int(this._capacity / 2)
		this._data = ReAllocate(this._data, this._capacity * SizeOf(String))
		memNull(this._data + this._count * SizeOf(String), Int(this._capacity / 2))
	EndIf
	#If __FB_DEBUG__
		Print "INFO: Adding element at index "; this._count; " with value "; value
		If this._data = 0 Then Print "ERROR: list.Add(value) cannot allocate memory: capacity: "; this._capacity
	#EndIf
	If this._data = 0 Then Return 'FIXME
	
	Print "before-Add: "; Str(*Cast(tString Ptr, this._data + this._count * SizeOf(String)))
	*Cast(String ptr, this._data + this._count * SizeOf(String)) = value
	Print "after-Add:  "; Str(*Cast(tString Ptr, this._data + this._count * SizeOf(String)))
	this._count += 1
End Sub

Operator stwListStr.[] (index As Integer) ByRef As String
	Return this.Item(index)
End Operator




'###############################################
'###                 TEST                    ###
'###############################################


Dim Shared l As stwListStr


l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")
l.Add("asdfasf")

Sub test()
	
	l.Add("sub")
	Dim s As String
	s = "function"
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
End Sub

test

For q As Integer = 0 To l.Count-1
	Print @l.Item(q)
	Print @l[q]
	Print l.Item(q)
	Print l[q]
Next

Sleep





