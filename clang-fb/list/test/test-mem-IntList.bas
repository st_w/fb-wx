' integer list test (memory corruption) using complete list header file

#Include "../stwList.bi"


defineList(Integer, Int)


Dim Shared l As stwListInt


l.Add(1)

Sub test()
	
	l.Add(2)
	l.Add(3)
	l.Add(4)
	l.Add(5)
	l.Add(6)
	l.Add(7)
	l.Add(8)
	Dim As Integer a,b,c,d,e,f,g,h,i
	a = 9
	b = 10
	c = 11
	d = 12
	e = 13
	f = 14
	g = 15
	h = 16
	i = 17
	l.Add(a)
	l.Add(b)
	l.Add(c)
	l.Add(d)
	l.Add(e)
	l.Add(f)
	l.Add(g)
	l.Add(h)
	l.Add(i)
End Sub

test

For q As Integer = 0 To l.Count-1
	Print l[q]
Next

Sleep





