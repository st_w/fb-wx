' Low level examples of UDT as return value
' inteded for being compiled with parameter -s to generate .asm file

Type TType
	a As Integer
	b As Integer
	c As Integer
	d As Integer
End Type


Function test() As TType
	' Stack layout:
	' [ebp+24] retval TType.d
	' [ebp+20] retval TType.c
	' [ebp+16] retval TType.b
	' [ebp+12] retval TType.a
	' [ebp+ 8] @TType => [ebp+12]
	' [ebp+ 4] RETURN
	' [ebp+ 0] ebp
	' [ebp- 4] local t.d
	' [ebp- 8] local t.c
	' [ebp-12] local t.b
	' [ebp-16] local t.a
	Dim t As TType
	t.d = 1
	
	'adress of TType instance on stack [ebp+ 8] is returned
	'ret 4 removes hidden parameter (@TType return value)
	Return t
End Function


'instance of TType is created (on stack)
'address of that TType is pushed on stack
'method test() is called and modifies that instance of TType on stack
Print test().a