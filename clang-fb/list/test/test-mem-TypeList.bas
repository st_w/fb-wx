' UDT list test (memory consumption, corruption) using complete header file

#Include "../stwList.bi"


Type simpleType
	a As Integer
	b As Integer
	c As Integer
	Declare Operator Let(a As simpleType)
	Declare Operator Cast() As String
End Type

Operator simpleType.Let (a As simpleType)
	this.a = a.a
	this.b = a.b
	this.c = a.c
End Operator

Operator simpleType.Cast() As String
	Return Str(a) + " " + Str(b) + " " + Str(c)
End Operator

Operator =(l As simpleType, r As simpleType) As Integer
	If l.a <> r.a Then Return 0
	If l.b <> r.b Then Return 0
	If l.c <> r.c Then Return 0
	Return -1
End Operator



defineList(simpleType, UDT)


Dim Shared l As stwListUDT


l.Add(Type<simpleType> (1,2,3))
l.Add(Type<simpleType> (1,2,3))
l.Add(Type<simpleType> (1,2,3))
l.Add(Type<simpleType> (1,2,3))
l.Add(Type<simpleType> (1,2,3))
l.Add(Type<simpleType> (1,2,3))
l.Add(Type<simpleType> (1,2,3))
l.Add(Type<simpleType> (1,2,3))
l.Add(Type<simpleType> (1,2,3))

Sub test()
	
	l.Add(Type<simpleType> (10,20,30))
	Dim s As simpleType
	s.a = 50
	s.b = 100
	s.c = 200
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
	l.Add(s)
End Sub

test

For q As Integer = 0 To l.Count-1
	Print Str(l.Item(q))
	Print Str(l[q])
Next



'change value in list (byref)
l[0].b = 12345
Print "1: "; l[0].b


'do not change value in list via other variable
Dim a As simpleType
a = l[0]
a.b = 67890
Print "2: "; l[0].b


Sleep







/'



'''''''''''''''''''
Print "test memory consumption of fill, clear"

Dim l2 As stwListStr

For q As Integer = 0 To 300000
	l2.Add("abcdefghijklmnopqrstuvwxyz")
Next

l2.Clear_()

'' ensures that array is set to 0
'' needs _data to be public (in stwList)
'For q As Integer = 0 To 300000
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[0] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[1] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[2] <> 0 Then Print q
'Next


l2.TrimExcess()

Print l2.Count



'''''''''''''''''''
Print "test memory consumption of fill, remove (single)"

For q As Integer = 0 To 13000
	l2.Add("abcdefghijklmnopqrstuvwxyz")
	'Print *Cast(UInteger Ptr, @l2[q])			' allocated string data addresses
Next

Print "removing .."
For q As Integer = 0 To 13000
'	If q Mod 10000 = 0 Then Print q
	l2.RemoveAt(0)
Next

Print l2.Count

'' ensures that array is set to 0
'' needs _data to be public (in stwList)
'For q As Integer = 0 To 13000
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[0] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[1] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[2] <> 0 Then Print q
'Next

Print "trimming .."
l2.TrimExcess()

Sleep



'''''''''''''''''''
Print "test memory consumption of fill, remove (range)"

For q As Integer = 1 To 10000
	l2.Add("abcdefghijklmnopqrstuvwxyz")
	'Print *Cast(UInteger Ptr, @l2[q])			' allocated string data addresses
Next

Print "removing .."
For q As Integer = 1 To 1000
'	If q Mod 10000 = 0 Then Print q
	l2.RemoveRange(0, 10)
Next

Print l2.Count

'' ensures that array is set to 0
'' needs _data to be public (in stwList)
'For q As Integer = 0 To 9999
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[0] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[1] <> 0 Then Print q
'	If Cast(UInteger Ptr, @(*Cast(String Ptr, l2._data + q*SizeOf(String))))[2] <> 0 Then Print q
'Next

Print "trimming .."
l2.TrimExcess()

Print l2.Count





Sleep




'/
