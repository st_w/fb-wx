' Low level memory operations on strings

Dim a As String
a = ""
a = ""


Sub memNull naked StdCall (addr As any Ptr, sz As UInteger)
	' stack layout:
	' [esp+ 8] sz
	' [esp+ 4] addr
	' [esp+ 0] RETURN
	Asm
		'push    edi             ' proc uses edi, so save it.
		mov edx, edi
		
		Xor     eax, eax        ' store zeros (only al used)
		mov     ecx, [esp + 8] ' sz As UInteger
		mov     edi, [esp + 4]  ' addr As Any Ptr
		rep     stosb 
		
		mov edi, edx
		'mov     eax, [esp + 4]  ' return pointer
		'pop     edi             ' restore edi
		ret 8                    ' adjust stack ourselfes
	End Asm
End Sub


Sub memNull2 naked StdCall (addr As any Ptr, sz As UInteger)
	' stack layout:
	' [esp+12] sz
	' [esp+ 8] addr
	' [esp+ 4] RETURN
	' [esp+ 0] edi backup
	Asm
		push    edi             ' proc uses edi, so save it.
		'mov edx, edi
		
		Xor     eax, eax        ' store zeros (only al used)
		mov     ecx, [esp + 12] ' sz As UInteger
		mov     edi, [esp + 8]  ' addr As Any Ptr
		rep     stosb 
		
		'mov edi, edx
		'mov     eax, [esp + 4]  ' return pointer
		pop     edi             ' restore edi
		ret 8                    ' adjust stack ourselfes
	End Asm
End Sub


Sub memNull3 naked Cdecl (addr As any Ptr, sz As UInteger)
	' stack layout:
	' [esp+12] sz
	' [esp+ 8] addr
	' [esp+ 4] RETURN
	' [esp+ 0] edi backup
	Asm
		push    edi             ' proc uses edi, so save it.
		'mov edx, edi
		
		Xor     eax, eax        ' store zeros (only al used)
		mov     ecx, [esp + 12] ' sz As UInteger
		mov     edi, [esp + 8]  ' addr As Any Ptr
		rep     stosb 
		
		'mov edi, edx
		'mov     eax, [esp + 4]  ' return pointer
		pop     edi             ' restore edi
		ret                     ' let caller adjust stack
	End Asm
End Sub


a = "Hallo Welt"
Print a
Dim As UByte Ptr tmp = StrPtr(a)
Dim As Double start, ende

start = Timer
For q As Integer = 0 To 100000000
	memNull(tmp, 6)
Next
ende = Timer
Print ende-start

start = Timer
For q As Integer = 0 To 100000000
	memNull2(tmp, 6)
Next
ende = Timer
Print ende-start


start = Timer
For q As Integer = 0 To 100000000
	memNull3(tmp, 6)
Next
ende = Timer
Print ende-start

Print a

Sleep

