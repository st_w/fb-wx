' samples for String allocation / deallocation

#Include "../stwList.bi"

Type tString
	addr As Integer
	sz As Integer
	alloc As Integer
	Declare Operator Cast() As String
End Type

Operator tString.Cast() As String
	Return Str(@this) + " (addr: " + Str(addr) + ", sz: " + Str(sz) + ", alloc: " + Str(alloc) + ")"
End Operator



Type Tt
	Dim b As String
	Dim c As String
	Dim d As String
	Dim e As tString
End Type

Dim Shared t As Tt



Sub stralloc(tmp As String)
	Print "tmp "; Str(*Cast(tString Ptr, @tmp))
	Dim a As String
	a = "Hallo Welt"
	Print "a "; Str(*Cast(tString Ptr, @a))
	t.b = a
	t.c = tmp
	*Cast(String ptr, @t.d) = tmp
	*Cast(String ptr, @t.e) = tmp
End Sub

stralloc("Hello")

Print "b "; Str(*Cast(tString Ptr, @t.b))
Print "c "; Str(*Cast(tString Ptr, @t.c))
Print "d "; Str(*Cast(tString Ptr, @t.d))
Print "e "; Str(*Cast(tString Ptr, @t.e))



Dim s As String
Print "s "; Str(*Cast(tString Ptr, @s))
s = ""
Print "s "; Str(*Cast(tString Ptr, @s))
s = "a"
Print "s "; Str(*Cast(tString Ptr, @s))
s = ""
Print "s "; Str(*Cast(tString Ptr, @s))

s = "Hallo"
fb_StrDelete(s)
Print "s "; Str(*Cast(tString Ptr, @s))





Sleep
End





defineList(String, Str)


Dim Shared l As stwListStr

