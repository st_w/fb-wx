//#include "interface/wx/defs.h"
//#include "interface/wx/chartype.h"
//#include "interface/wx/string.h"
//#include "interface/wx/versioninfo.h"


#include "interface/wx/defs.h"
#include "interface/wx/object.h"
#include "interface/wx/dynarray.h"
#include "interface/wx/list.h"
#include "interface/wx/hash.h"
#include "interface/wx/string.h"
#include "interface/wx/hashmap.h"
#include "interface/wx/arrstr.h"
#include "interface/wx/intl.h"
#include "interface/wx/log.h"
#include "interface/wx/event.h"
#include "interface/wx/app.h"
#include "interface/wx/utils.h"
#include "interface/wx/stream.h"
#include "interface/wx/memory.h"
#include "interface/wx/math.h"
#include "interface/wx/stopwatch.h"
#include "interface/wx/timer.h"
#include "interface/wx/module.h"
#include "interface/wx/wxcrt.h"
#include "interface/wx/wxcrtvararg.h"

#if wxUSE_GUI

#include "interface/wx/window.h"
#include "interface/wx/containr.h"
#include "interface/wx/panel.h"
#include "interface/wx/toplevel.h"
#include "interface/wx/frame.h"
#include "interface/wx/gdicmn.h"
#include "interface/wx/gdiobj.h"
#include "interface/wx/region.h"
#include "interface/wx/bitmap.h"
#include "interface/wx/image.h"
#include "interface/wx/colour.h"
#include "interface/wx/font.h"
#include "interface/wx/dc.h"
#include "interface/wx/dcclient.h"
#include "interface/wx/dcmemory.h"
#include "interface/wx/dcprint.h"
#include "interface/wx/dcscreen.h"
#include "interface/wx/button.h"
#include "interface/wx/menuitem.h"
#include "interface/wx/menu.h"
#include "interface/wx/pen.h"
#include "interface/wx/brush.h"
#include "interface/wx/palette.h"
#include "interface/wx/icon.h"
#include "interface/wx/cursor.h"
#include "interface/wx/dialog.h"
#include "interface/wx/settings.h"
#include "interface/wx/msgdlg.h"
#include "interface/wx/dataobj.h"

#include "interface/wx/control.h"
#include "interface/wx/ctrlsub.h"
#include "interface/wx/bmpbuttn.h"
#include "interface/wx/checkbox.h"
#include "interface/wx/checklst.h"
#include "interface/wx/choice.h"
#include "interface/wx/scrolbar.h"
#include "interface/wx/stattext.h"
#include "interface/wx/statbmp.h"
#include "interface/wx/statbox.h"
#include "interface/wx/listbox.h"
#include "interface/wx/radiobox.h"
#include "interface/wx/radiobut.h"
#include "interface/wx/textctrl.h"
#include "interface/wx/slider.h"
#include "interface/wx/gauge.h"
#include "interface/wx/scrolwin.h"
#include "interface/wx/dirdlg.h"
#include "interface/wx/toolbar.h"
#include "interface/wx/combobox.h"
#include "interface/wx/layout.h"
#include "interface/wx/sizer.h"
#include "interface/wx/statusbr.h"
#include "interface/wx/choicdlg.h"
#include "interface/wx/textdlg.h"
#include "interface/wx/filedlg.h"

// this one is included by exactly one file (mdi.cpp) during wx build so even
// although we keep it here for the library users, don't include it to avoid
// bloating the PCH and (worse) rebuilding the entire library when it changes
// when building the library itself
#ifndef WXBUILDING
    #include "interface/wx/mdi.h"
#endif

// always include, even if !wxUSE_VALIDATORS because we need wxDefaultValidator
#include "interface/wx/validate.h"

#if wxUSE_VALIDATORS
    #include "interface/wx/valtext.h"
#endif // wxUSE_VALIDATORS

#endif // wxUSE_GUI

