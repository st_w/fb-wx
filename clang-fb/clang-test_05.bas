
#Inclib "clang"
#Include "clang.bi"
#Include "list/stwList.bi"

Declare Function getCursorKindStr(kind As CXCursorKind) As String
Declare Function getAccessSpecifierStr(accSpec As CX_CXXAccessSpecifier) As String
Declare Function clean_path(filepath As String) As String

defineList(String, Str, 0)

Dim Shared wxHeadersLocation As String
wxHeadersLocation = clean_path("E:\wx\wx\wx301\")


#Define NL Chr(13,10)



'CXString helper
Function getCXS(ByRef s As CXString) As String
	Dim As String tmp = *clang_getCString(s)
	clang_disposeString(s)
	Return tmp
End Function

Function strStartsWith(ByRef baseStr As String, ByRef startStr As String) As Integer
	If Len(baseStr) < Len(startStr) Then Return 0
	For q As Integer = 0 To Len(startStr)-1
		If baseStr[q] <> startStr[q] Then Return 0
	Next
	Return -1
End Function





'**************** Namespace *********************



Declare Function getNS() As String



Dim Shared As String curNS



'**************** Object Stack *********************

Type TObj
	
	cur As CXCursor
	obj As Any Ptr
		
End Type



Dim Shared As TObj ObjectStack(0 To 100)
Dim Shared As Integer ObjectStackCnt = 0


Sub pushObj(obj As TObj)
	ObjectStack(ObjectStackCnt) = obj
	ObjectStackCnt+=1
	If obj.cur.kind = CXCursor_Namespace Then curNS = getNS()
End Sub

Sub popObj (n As Integer = 1)
	Dim mustRefreshNS As Integer = 0
	For q As Integer = ObjectStackCnt-1 To ObjectStackCnt-n Step -1
		If ObjectStack(q).cur.kind = CXCursor_Namespace Then mustRefreshNS = 1
	Next
	ObjectStackCnt-=n
	If mustRefreshNS Then curNS = getNS()
End Sub

Function poprObj (n As Integer = 1) As TObj
	popObj(n)
	Return ObjectStack(ObjectStackCnt+1)
End Function




'**************** EnumValue *********************


Type TEnumValue
	
	Dim nam As String
	Dim value As UInteger
	
	Dim cur As CXCursor
	
	Declare Operator Cast() As String
	
End Type


Operator TEnumValue.Cast() As String
	Return nam + ":" + Str(value)
End Operator

Operator = ( ByRef lhs As TEnumValue, ByRef rhs As TEnumValue ) As Integer
	Return lhs.value = rhs.value
End Operator

Operator <> ( ByRef lhs As TEnumValue, ByRef rhs As TEnumValue ) As Integer
	Return (lhs = rhs) = 0 
End Operator



defineList(TEnumValue, EnumValue, 1)




'**************** Enum *********************


Type TEnum
	
	'Name
	Dim nam As String
	Dim values As stwListEnumValue
	
	Dim acc As CX_CXXAccessSpecifier
	Dim cur As CXCursor
	
	Declare Operator Cast() As String
	
End Type


Operator TEnum.Cast() As String
	Return nam
End Operator

Operator = ( ByRef lhs As TEnum, ByRef rhs As TEnum ) As Integer
	Return lhs.nam = rhs.nam
End Operator

Operator <> ( ByRef lhs As TEnum, ByRef rhs As TEnum ) As Integer
	Return (lhs = rhs) = 0 
End Operator



defineList(TEnum, Enum, 1)






'**************** Parameter (Method) *********************


Type TParam
	
	'Name
	Dim nam As String
	Dim typ As String
	Dim defVal As String
	
	Dim xtyp As String			'type for export
	
	Dim cur As CXCursor
	
	Declare Operator Cast() As String
	
End Type


Operator TParam.Cast() As String
	Return nam + ":" + typ
End Operator

Operator = ( ByRef lhs As TParam, ByRef rhs As TParam ) As Integer
	Return lhs.nam = rhs.nam andalso lhs.typ = rhs.typ
End Operator

Operator <> ( ByRef lhs As TParam, ByRef rhs As TParam ) As Integer
	Return (lhs = rhs) = 0 
End Operator



defineList(TParam, Param, 1)





'**************** Method *********************


Enum CX_InheritanceSpecifier
	CX_NormalInh,
	CX_Virtual,
	CX_PureVirtual,
	CX_Static,
	CX_InvalidInh
End Enum


Type TMethod
	
	'Name
	Dim nam As String
	Dim xnam As String	'exported name
	Dim vnam As String	'virtual function callback member variable
	Dim vtyp As String	'virtual function callback type
	Dim vOverride As Integer = 0				'isVirtual or protected -> do override in wrapper class
	
	
	Dim inh As CX_InheritanceSpecifier
	Dim acc As CX_CXXAccessSpecifier
	Dim isConst As Integer = 0
	
	Dim cur As CXCursor
	
	Dim ret As String
	Dim xret As String			'becomes void, when returning as parameter
	Dim retAsParam As Integer = 0			'return value as byref parameter
	
	Dim params As stwListParam
	
	Declare Operator Cast() As String
	
End Type


Operator TMethod.Cast() As String
	Return nam
End Operator

Operator = ( ByRef lhs As TMethod, ByRef rhs As TMethod ) As Integer
	Return lhs.nam = rhs.nam
End Operator

Operator <> ( ByRef lhs As TMethod, ByRef rhs As TMethod ) As Integer
	Return (lhs = rhs) = 0 
End Operator



defineList(TMethod, Meth, 1)





'**************** Field *********************


Type TField
	
	'Name
	Dim nam As String
	
	Dim getter As String
	Dim setter As String
	
	Dim acc As CX_CXXAccessSpecifier
		
	Dim cur As CXCursor
	
	Declare Operator Cast() As String
	
End Type


Operator TField.Cast() As String
	Return nam
End Operator

Operator = ( ByRef lhs As TField, ByRef rhs As TField ) As Integer
	Return lhs.nam = rhs.nam
End Operator

Operator <> ( ByRef lhs As TField, ByRef rhs As TField ) As Integer
	Return (lhs = rhs) = 0 
End Operator



defineList(TField, Field, 1)




'**************** Class / Struct *********************


Type TClassForward As TClass
declareList(TClassForward, Class)
declareList(TClassForward Ptr, ClassPtr)

Type TClass
	
	'Namespace
	Dim ns As String
	'Name
	Dim nam As String
	Dim vnam As String	'name of wrapper class for providing "virtual" methods
	Dim pnam As String	'name used for any ptr to the external class
	
	Dim referenced As Integer = 0
	Dim inheritable As Integer = 0
	
	Dim virt As Integer
	
	Dim baseClasses As stwListClassPtr
	Dim baseClassesStr As stwListStr
	Dim innerClasses As stwListClass
	
	Dim methods As stwListMeth
	Dim ctors As stwListMeth
	Dim dtor As TMethod
	Dim fields As stwListField
	Dim enums As stwListEnum
	
	Dim cur As CXCursor
	
	Declare Operator Cast() As String
	
	
	'Declare Constructor()
	'Declare Constructor(other As TClass)
	
End Type


'Constructor TClass()
'
'End Constructor
'
'
'Constructor TClass(other As TClass)
'	this.ns = other.ns
'	this.nam = other.nam
'	this.referenced = other.referenced
'	this.virt = other.virt
'	this.innerClasses = other.innerClasses
'End Constructor




Operator TClass.Cast() As String
	Return ns + "::" + nam
End Operator

Operator = ( ByRef lhs As TClass, ByRef rhs As TClass ) As Integer
	Return lhs.nam = rhs.nam AndAlso lhs.ns = rhs.ns
End Operator

Operator <> ( ByRef lhs As TClass, ByRef rhs As TClass ) As Integer
	Return (lhs = rhs) = 0 
End Operator

implementList(TClassForward, Class, 1)
implementList(TClassForward Ptr, ClassPtr, 0)



Type TClassVMethodIterator
	
	Private:
	Dim c As TClass Ptr
	Dim i As Integer = -1
	
	Public:
	Declare Property Current() ByRef As TMethod
	Declare Property CurClass() ByRef As TClass
	Declare Sub Reset_()
	Declare Function MoveNext() As Integer
	Declare Constructor(c As TClass Ptr)
	
End Type

Constructor TClassVMethodIterator (c As TClass Ptr)
	If c = 0 Then Print "ERROR: TClassVMethodIterator - Class Parameter must not be null!"
	this.c = c
End Constructor

Property TClassVMethodIterator.Current() ByRef As TMethod
	Return c->methods[i]
End Property

Property TClassVMethodIterator.CurClass() ByRef As TClass
	Return *c
End Property

Function TClassVMethodIterator.MoveNext() As Integer
	While i < c->methods.Count-1
		i+=1
		If this.Current.vOverride Then Return 1
	Wend
	Return 0
End Function

Sub TClassVMethodIterator.Reset_()
	i = -1
End Sub








Dim Shared Classes As stwListClass



Function findClass(nam As String, ns As String, clist As stwListClass Ptr = @Classes) As Integer
	For q As Integer = 0 To clist->Count-1
		If (*clist)[q].nam = nam AndAlso (*clist)[q].ns = ns Then Return q
	Next
	Return -1
End Function


'**************** Namespace impl *********************

Function getNS() As String
	Dim ns As String
	For q As Integer = ObjectStackCnt-1 To 0 Step -1
		If ObjectStack(q).cur.kind = CXCursor_Namespace Then
			Dim As CXString cxstrSpelling = clang_getCursorSpelling(ObjectStack(q).cur)
			If ns = "" Then
				ns = *clang_getCString(cxstrSpelling)
			Else
				ns = *clang_getCString(cxstrSpelling) + "::" + ns
			EndIf
			clang_disposeString(cxstrSpelling)
		EndIf
	Next
	Return ns
End Function


Function getParentClass() As TClass Ptr
	Dim ns As String
	For q As Integer = ObjectStackCnt-2 To 0 Step -1
		If ObjectStack(q).cur.kind = CXCursor_ClassDecl OrElse ObjectStack(q).cur.kind = CXCursor_StructDecl Then
			Return ObjectStack(q).obj
		EndIf
	Next
	Return 0
End Function




'==================================================================



Operator = ( ByRef lhs As CXCursor, ByRef rhs As CXCursor ) As Integer
	'Return lhs.kind = rhs.kind And lhs.xdata = rhs.xdata And lhs.data(0) = rhs.data(0) And lhs.data(1) = rhs.data(1) And lhs.data(2) = rhs.data(2)
	Return clang_equalCursors(lhs, rhs)
End Operator

Operator <> ( ByRef lhs As CXCursor, ByRef rhs As CXCursor ) As Integer
	Return (lhs = rhs) = 0 
End Operator


'type CXCursorVisitor as function(byval cursor as CXCursor, byval parent as CXCursor, byval client_data as CXClientData) as CXChildVisitResult
Function TUVisitor Cdecl (byval cursor as CXCursor, byval parent as CXCursor, byval client_data as CXClientData) as CXChildVisitResult
	
	Dim As CXSourceLocation cxloc = clang_getCursorLocation(cursor)
	Dim As CXFile cxfile
	Dim As Integer ln, col, offs
	clang_getSpellingLocation(cxloc, @cxfile, @ln, @col, @offs)
	Dim As String fileName = clean_path(getCXS( clang_getFileName(cxfile) ))
	
	'Print filename
	If strStartsWith(fileName, wxHeadersLocation) = 0 Then Return CXChildVisit_Continue
	
	'Templates not supported
	If cursor.kind = CXCursor_FunctionTemplate OrElse cursor.kind = CXCursor_ClassTemplate Then Return CXChildVisit_Continue
	
	
	Dim foundCur As Integer = 0
	For q As Integer = ObjectStackCnt-1 To 0 Step -1
		If parent = ObjectStack(q).cur Then
			Dim tmpObj As TObj
			tmpObj.cur = cursor
			popObj(ObjectStackCnt-q-1)
			pushObj(tmpObj)
			foundCur = -1
			Exit For
		EndIf
	Next
	If foundCur = 0 Then
		Print " WARNING: parent unknown !!"
		ObjectStack(0).cur = cursor
		ObjectStackCnt = 1
	EndIf
	
	If cursor.kind = CXCursor_ClassDecl OrElse cursor.kind = CXCursor_StructDecl Then
		Dim As stwListClass Ptr clist = @Classes
		Dim As String className = getCXS( clang_getCursorSpelling(cursor) )
		
		Dim As TClass Ptr parentCls = getParentClass()
		If parentCls <> 0 Then clist = @(parentCls->innerClasses)
		
		Dim As Integer classIdx = findClass(className, curNS, clist)
				
		Dim tmpCls As TClass
		If classIdx < 0 Then
			tmpCls.nam = className
			tmpCls.ns = curNS
			tmpCls.cur = cursor
			clist->Add(tmpCls)
			ObjectStack(ObjectStackCnt-1).obj = @((*clist)[clist->Count-1])
		Else
			ObjectStack(ObjectStackCnt-1).obj = @((*clist)[classIdx])
		EndIf
		
		
		
	ElseIf cursor.kind = CXCursor_CXXMethod Or cursor.kind = CXCursor_Constructor Or cursor.kind = CXCursor_Destructor Then
		
		Dim As TClass Ptr pClass = getParentClass()
		If pClass <> 0 Then
			Dim As TMethod tmpMeth
			tmpMeth.nam = getCXS( clang_getCursorSpelling(cursor) )
			tmpMeth.acc = clang_getCXXAccessSpecifier(cursor)
			tmpMeth.inh = CX_NormalInh
			tmpMeth.cur = cursor
			tmpMeth.ret = getCXS(clang_getTypeSpelling(clang_getCursorResultType(cursor)))
			If (clang_CXXMethod_isVirtual(cursor)) Then tmpMeth.inh = CX_Virtual
			If (clang_CXXMethod_isPureVirtual(cursor)) Then tmpMeth.inh = CX_PureVirtual
			If (clang_CXXMethod_isStatic(cursor)) Then tmpMeth.inh = CX_Static
			If (clang_CXXMethod_isConst(cursor)) Then tmpMeth.isConst = 1
			If tmpMeth.inh = CX_Virtual OrElse tmpMeth.inh = CX_PureVirtual OrElse tmpMeth.acc = CX_CXXProtected Then tmpMeth.vOverride = 1
			If cursor.kind = CXCursor_CXXMethod Then
				If tmpMeth.acc <> CX_CXXPublic And tmpMeth.acc <> CX_CXXProtected Then Return CXChildVisit_Continue
				If tmpMeth.acc = CX_CXXProtected Then pClass->inheritable = 1
				pClass->methods.Add(tmpMeth)
				ObjectStack(ObjectStackCnt-1).obj = @(pClass->methods[pClass->methods.Count-1])
			ElseIf cursor.kind = CXCursor_Constructor Then
				pClass->ctors.Add(tmpMeth)
				ObjectStack(ObjectStackCnt-1).obj = @(pClass->ctors[pClass->ctors.Count-1])
			ElseIf cursor.kind = CXCursor_Destructor Then
				pClass->dtor = tmpMeth
				ObjectStack(ObjectStackCnt-1).obj = @(pClass->dtor)
			EndIf
			
		EndIf
	
	ElseIf cursor.kind = CXCursor_FieldDecl Then
	
		Dim As TClass Ptr pClass = getParentClass()
		If pClass <> 0 Then
			Dim As TField tmpFld
			tmpFld.nam = getCXS( clang_getCursorSpelling(cursor) )
			tmpFld.acc = clang_getCXXAccessSpecifier(cursor)
			If tmpFld.acc <> CX_CXXPublic And tmpFld.acc <> CX_CXXProtected Then Return CXChildVisit_Continue
			If tmpFld.acc = CX_CXXProtected Then pClass->inheritable = 1
			tmpFld.cur = cursor
			pClass->fields.Add(tmpFld)
			
		EndIf
	
	ElseIf cursor.kind = CXCursor_CXXBaseSpecifier Then
		
		Dim As TClass Ptr pClass = getParentClass()
		If pClass <> 0 Then
			Dim As String baseClassSpec = getCXS( clang_getCursorSpelling(cursor) )
			If Left(baseClassSpec, 6) = "class " Then
				pClass->baseClassesStr.Add( Mid(baseClassSpec, 7))
			ElseIf Left(baseClassSpec, 7) = "struct " Then
				pClass->baseClassesStr.Add( Mid(baseClassSpec, 8))
			Else	'templates, etc.
				pClass->baseClassesStr.Add( baseClassSpec )
			EndIf
		EndIf
	
	ElseIf cursor.kind = CXCursor_ParmDecl Then
		
		If ObjectStackCnt-1 > 0 AndAlso (ObjectStack(ObjectStackCnt-2).cur.kind = CXCursor_CXXMethod  Or ObjectStack(ObjectStackCnt-2).cur.kind = CXCursor_Constructor Or ObjectStack(ObjectStackCnt-2).cur.kind = CXCursor_Destructor) Then
			Dim As TMethod Ptr pMeth = ObjectStack(ObjectStackCnt-2).obj
			If pMeth <> 0 Then
				Dim As TParam tmpParam
				tmpParam.nam = getCXS(clang_getCursorSpelling(cursor))
				tmpParam.typ = getCXS(clang_getTypeSpelling(clang_getCursorType(cursor)))
				tmpParam.cur = cursor
				pMeth->params.Add(tmpParam)
			EndIf
		EndIf
	
	ElseIf cursor.kind = CXCursor_EnumDecl Then
		
		Dim As TClass Ptr pClass = getParentClass()
		If pClass <> 0 Then
			Dim As TEnum tmpEnum
			tmpEnum.nam = getCXS( clang_getCursorSpelling(cursor) )
			tmpEnum.cur = cursor
			tmpEnum.acc = clang_getCXXAccessSpecifier(cursor)
			If tmpEnum.acc <> CX_CXXPublic And tmpEnum.acc <> CX_CXXProtected Then Return CXChildVisit_Continue
			If tmpEnum.acc = CX_CXXProtected Then pClass->inheritable = 1
			pClass->enums.Add(tmpEnum)
			ObjectStack(ObjectStackCnt-1).obj = @(pClass->enums[pClass->enums.Count-1])
		EndIf
	
	
	ElseIf cursor.kind = CXCursor_EnumConstantDecl Then
		
		If ObjectStackCnt-2 >= 0 AndAlso ObjectStack(ObjectStackCnt-2).cur.kind = CXCursor_EnumDecl Then
			Dim As TEnum Ptr pEnum = ObjectStack(ObjectStackCnt-2).obj
			If pEnum <> 0 Then
				Dim As TEnumValue tmpEnumVal
				tmpEnumVal.nam = getCXS(clang_getCursorSpelling(cursor))
				tmpEnumVal.value = clang_getEnumConstantDeclUnsignedValue(cursor)
				tmpEnumVal.cur = cursor
				pEnum->values.Add(tmpEnumVal)
			EndIf
		EndIf
		
	
	EndIf


	
	'If cursor.kind = CXCursor_ClassDecl Then
		
		
		'Print Space((ObjectStackCnt-2)*2); "- ";tmp; " (line:"; ln; ", col:"; col; ", offs:"; offs; ")"
		
		Dim As CXString cxstrDisplayName = clang_getCursorDisplayName(cursor)
		Dim As CXString cxstrSpelling = clang_getCursorSpelling(cursor)
		'Print Space((ObjectStackCnt-2)*2); "# "; *clang_getCString(cxstrDisplayName); "  "; *clang_getCString(cxstrSpelling), getCursorKindStr(cursor.kind)
		'If cursor.kind = CXCursor_CXXMethod Then
		'	Print getAccessSpecifierStr(clang_getCXXAccessSpecifier(cursor))
		'	Dim As CXType funcType = clang_getCursorType(cursor)
		'	For q As Integer = 0 To clang_Cursor_getNumArguments(cursor)-1
		'		Print "~~ param "; q; "  "; getCXS(clang_getTypeSpelling(clang_getArgType(funcType, q)))
		'	Next
		'		Print "~~ retval "; getCXS(clang_getTypeSpelling(clang_getCursorResultType(cursor)))
		'EndIf
		'Print cursor.kind, cursor.xdata, cursor.data(0), cursor.data(1), cursor.data(2)
		'Print parent.kind, parent.xdata, parent.data(0), parent.data(1), parent.data(2)
		'Print "# "; *clang_getCString(cxstrDisplayName)
		'Print "     -kind: "; getCursorKindStr(cursor.kind)
		clang_disposeString(cxstrDisplayName)
		clang_disposeString(cxstrSpelling)
		
		
		'cxstrDisplayName = clang_Cursor_getRawCommentText(cursor)
		'Print " - "; *clang_getCString(cxstrDisplayName)
		'clang_disposeString(cxstrDisplayName)
		
		Dim cxExt As CXSourceRange = clang_getCursorExtent(cursor)
		
		
	'EndIf
	
	
	Return CXChildVisit_Recurse
	
End Function



Dim As CXString cxverstr = clang_getClangVersion()
Print "Version: "; *clang_getCString(cxverstr)
clang_disposeString(cxverstr)
Print

Dim args(0 To 10) As ZString Ptr
args(0) = @"-c"
args(1) = @"-x"
args(2) = @"c++"
args(3) = @"-std=gnu++11"
args(4) = @"-Wno-undefined-inline"		'a lot of such warnings (TODO: why?) - hide them; no errors/warnings should remain
args(5) = @"-I"
args(6) = @"E:\wx\wx\wx301\include"
args(7) = @"-I"
args(8) = @"E:\wx\wx\wx301\lib\vc100_dll\mswud"
args(9) = @"-I"
args(10)= @"E:\wx\clang\LLVM-3.6.0-r214711-win32\lib\clang\3.6.0\include"
'args(10) = @"-I"
'args(11) = @"C:\Program Files (x86)\Microsoft SDKs\Windows\v7.1A\Include" '"C:\mingw32\i686-w64-mingw32\include"
'args(12) = @"-I"
'args(13) = @"C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\include"

'check with clang++ -v ; see http://stackoverflow.com/questions/12304319/clang-and-c11-headers
'-nostdinc++ -nostdsysteminc -nobuiltininc

'args(9) = @"-fms-extensions"
'args(10)= @"-fms-compatibility"
'args(7) = @"-Ddummydef123"
'args(5) = @"dummydef123"
'args(2) = @"-DwxUSE_GUI"

'E:\wxw3_headers\include\wx\wx.h
Dim As CXIndex cidx = clang_createIndex(0, 0)
Dim As CXTranslationUnit_Flags cxflags = CXTranslationUnit_SkipFunctionBodies 'or CXTranslationUnit_DetailedPreprocessingRecord
Dim As CXTranslationUnit cxtu = clang_parseTranslationUnit(cidx, @"wxdummy.cxx", @args(0), UBound(args)+1, NULL, 0, cxflags)
'Dim As CXTranslationUnit cxtu = clang_parseTranslationUnit(cidx, @"c++/test-class-method.cpp", @args(0), UBound(args)+1, NULL, 0, cxflags)
Dim As CXCursor cxcu = clang_getTranslationUnitCursor(cxtu)
'this is the parent of all cursors
ObjectStack(0).cur = cxcu
ObjectStackCnt = 1

Dim As CXString cxtustr = clang_getTranslationUnitSpelling(cxtu)
Print "TU Spelling: "; *clang_getCString(cxtustr)
clang_disposeString(cxtustr)

clang_visitChildren(cxcu, @TUVisitor, 0)




Print
Print "ERRORS: "
For q As Integer = 0 to clang_getNumDiagnostics(cxtu)-1
	Dim As CXDiagnostic cxdiag = clang_getDiagnostic(cxtu, q)
	Dim As CXString cxdiagstr = clang_getDiagnosticSpelling(cxdiag)
	Print *clang_getCString(cxdiagstr)
	clang_disposeString(cxdiagstr)
	Dim As CXSourceLocation cxloc = clang_getDiagnosticLocation(cxdiag)
	Dim As CXFile cxloc_file
	Dim As ULong cxloc_line, cxloc_col, cxloc_offset
	clang_getSpellingLocation(cxloc, @cxloc_file, @cxloc_line, @cxloc_col, @cxloc_offset)
	cxdiagstr = clang_getFileName(cxloc_file)
	Print " in "; *clang_getCString(cxdiagstr); " Zeile "; cxloc_line; " Spalte "; cxloc_col; " Offset "; cxloc_offset
	clang_disposeString(cxdiagstr)
	Dim As CXDiagnosticDisplayOptions cxdiagflags = CXDiagnostic_DisplaySourceLocation Or CXDiagnostic_DisplayColumn Or  CXDiagnostic_DisplaySourceRanges or CXDiagnostic_DisplayOption or CXDiagnostic_DisplayCategoryId or CXDiagnostic_DisplayCategoryName
	cxdiagstr = clang_formatDiagnostic(cxdiag, cxdiagflags)
	Print " "; *clang_getCString(cxdiagstr)
	clang_disposeString(cxdiagstr)
Next



Print

/'
Dim As CXFile cxf = clang_getFile(cxtu, @"asdf.cpp")
Print "Non existing file: "; cxf
cxf = clang_getFile(cxtu, @"clang.bi")
Print "existing non-project file: "; cxf
cxf = clang_getFile(cxtu, @"c++/dummy.cpp")
Print "existing project file: "; cxf
'clang_isFileMultipleIncludeGuarded only works for #included files in translation unit
cxf = clang_getFile(cxtu, @"c++/fake-class.h")
Print "unprotected: "; clang_isFileMultipleIncludeGuarded(cxtu, cxf)
cxf = clang_getFile(cxtu, @"c++/fake-class-protected.h")
Print "protected: "; clang_isFileMultipleIncludeGuarded(cxtu, cxf)
'/










Function ReplaceAll(ByRef Daten As String, ByVal SearchPat As String, ByVal ReplacementStr As String) As String
	Dim q As Integer = 1
	
	Do
		q=InStr(q, Daten, SearchPat)
		If q=0 Then Exit Do
		daten = Left(Daten, q-1) & ReplacementStr & Mid(Daten, q+Len(SearchPat))
		q+=Len(ReplacementStr)
	Loop
	
	Return daten
End Function







Function prepare_Type(ByRef typ As String) As String
	Dim t As String = typ
	
	'WXW SPECIFIC HACK !! rename size_type to size_t
	If t = "size_type" Then t = "size_t"
	If t = "wxString::size_type" Then t = "size_t"
	
	'MSVC issue: replace [] by *
	t = ReplaceAll(t, "[]", "*")
	
	Return t
End Function









Function prepare_Methods(ByRef methods As stwListMeth) As Integer
	
	'make protected static methods virtual member methods
	For q As Integer = 0 To methods.Count-1
		If methods[q].vOverride And methods[q].inh = CX_Static Then
			methods[q].inh = CX_NormalInh
		EndIf
	Next

	
	
	'assign dummy parameter names for all empty parameters
	For q As Integer = 0 To methods.Count-1
		Dim As Integer anonParam = 1
		For w As Integer = 0 To methods[q].params.Count-1
			If methods[q].params[w].nam = "" Then
				methods[q].params[w].nam = "param" + Str(anonParam)
				Do
					For e As Integer = 0 To methods[q].params.Count-1
						If e <> w AndAlso methods[q].params[w].nam = methods[q].params[e].nam Then
							anonParam += 1
							methods[q].params[w].nam = "param" + Str(anonParam)
							Continue Do
						EndIf
					Next
					Exit Do
				Loop
				anonParam += 1
			EndIf
		Next
	Next
	
	'assign values to xret, params.xtyp
	For q As Integer = 0 To methods.Count-1
		methods[q].ret = prepare_Type(methods[q].ret)
		methods[q].xret = methods[q].ret
		For w As Integer = 0 To methods[q].params.Count-1
			methods[q].params[w].typ = prepare_Type(methods[q].params[w].typ)
			methods[q].params[w].xtyp = methods[q].params[w].typ
		Next
	Next
		
	
	Return 0
	
End Function



Function prepare_Class(ByRef c As TClass) As Integer
	
	'TEST: c.inheritable = 0
	
	
	'Add default constructor
	If c.ctors.Count = 0 Then 
		Dim As TMethod tmpCtor
		tmpCtor.nam = c.nam
		tmpCtor.inh = CX_NormalInh
		tmpCtor.acc = CX_CXXPublic
		c.ctors.Add(tmpCtor)
	EndIf
	
	'define exported names for ctors
	If c.ctors.Count = 1 Then
		c.ctors[0].xnam = c.nam + "_ctor"
	Else
		For q As Integer = 0 To c.ctors.Count-1
			c.ctors[q].xnam = c.nam + "_ctor" + Trim(Str(q))
		Next
	EndIf
	
	'define exported name for dtor
	c.dtor.xnam = c.nam + "_dtor"
	If c.dtor.vOverride Then
		c.dtor.vtyp = "Virtual_" + c.nam + "_onDispose"
		c.dtor.vnam = "cb_onDispose"
	EndIf
	
	'rename operator methods
	For q As Integer = 0 To c.methods.Count-1
		If Left(c.methods[q].nam, 8) <> "operator" Then Continue For
		Select Case Mid(c.methods[q].nam, 9)
			case "+": c.methods[q].xnam = "operator_plus"
			case "-": c.methods[q].xnam = "operator_minus"
			case "*": c.methods[q].xnam = "operator_star"
			case "/": c.methods[q].xnam = "operator_div"
			case "%": c.methods[q].xnam = "operator_mod"
			case "^": c.methods[q].xnam = "operator_exp"
			case "&": c.methods[q].xnam = "operator_and"
			case "|": c.methods[q].xnam = "operator_or"
			case "~": c.methods[q].xnam = "operator_not"
			case "!": c.methods[q].xnam = "operator_lnot"
			case "=": c.methods[q].xnam = "operator_assign"
			case "<": c.methods[q].xnam = "operator_lt"
			case ">": c.methods[q].xnam = "operator_gt"
			case "+=": c.methods[q].xnam = "operator_plus_assign"
			case "-=": c.methods[q].xnam = "operator_minus_assign"
			case "*=": c.methods[q].xnam = "operator_mul_assign"
			case "/=": c.methods[q].xnam = "operator_div_assign"
			case "%=": c.methods[q].xnam = "operator_mod_assign"
			case "^=": c.methods[q].xnam = "operator_exp_assign"
			case "&=": c.methods[q].xnam = "operator_and_assign"
			case "|=": c.methods[q].xnam = "operator_or_assign"
			case "<<": c.methods[q].xnam = "operator_shl"
			case ">>": c.methods[q].xnam = "operator_shr"
			case "<<=": c.methods[q].xnam = "operator_shl_assign"
			case ">>=": c.methods[q].xnam = "operator_shr_assign"
			case "==": c.methods[q].xnam = "operator_equ"
			case "!=": c.methods[q].xnam = "operator_nequ"
			case "<=": c.methods[q].xnam = "operator_lte"
			case ">=": c.methods[q].xnam = "operator_gte"
			case "&&": c.methods[q].xnam = "operator_land"
			case "||": c.methods[q].xnam = "operator_lor"
			case "++": c.methods[q].xnam = "operator_inc"
			case "--": c.methods[q].xnam = "operator_dec"
			case ",": c.methods[q].xnam = "operator_comma"
			case "->*": c.methods[q].xnam = "operator_pmember"
			case "->": c.methods[q].xnam = "operator_member"
			case "()": c.methods[q].xnam = "operator_call"
			case "[]": c.methods[q].xnam = "operator_index"
			case "new": c.methods[q].xnam = "operator_new"
			case "delete": c.methods[q].xnam = "operator_del"
			case "new[]": c.methods[q].xnam = "operator_newarr"
			case "delete[]": c.methods[q].xnam = "operator_delarr"
		End Select
	Next
	
	'define exported names for methods
	For q As Integer = 0 To c.methods.Count-1
		Dim As String namTemplate = IIf(c.methods[q].xnam <> "", c.methods[q].xnam, c.methods[q].nam)
		c.methods[q].xnam = namTemplate
		Dim As Integer dupCnt = 2
		Do
			For w As Integer = 0 To q-1
				If c.methods[w].xnam = c.methods[q].xnam Then
					c.methods[q].xnam = namTemplate + Str(dupCnt)
					dupCnt += 1
					Continue Do
				EndIf
			Next
			Exit Do
		Loop
		If c.methods[q].vOverride Then
			c.methods[q].vtyp = "Virtual_" + c.nam + "_" + c.methods[q].xnam
			c.methods[q].vnam = "cb_" + c.methods[q].xnam
		EndIf
	Next
	
	'resolve base classes
	For q As Integer = 0 To c.baseClassesStr.Count-1
		For w As Integer = 0 To Classes.Count-1
			If Classes[w].nam = c.baseClassesStr[q] Then c.baseClasses.Add(@Classes[w]) : Continue For, For
		Next
		Print "WARNING: base class not found for classname "; c.baseClassesStr[q]; " in Class "; c.nam
	Next
	
	
	prepare_Methods(c.methods)
	prepare_Methods(c.ctors)
	
	
	''Add default destructor
	'If c.dtor.nam = "" Then
	'	c.dtor.nam = "~" + c.nam
	'	c.dtor.inh = CX_NormalInh
	'	c.dtor.acc = CX_CXXPublic
	'EndIf	
	
	Return 0
End Function






Sub prepare_global(ByRef clist As stwListClass = Classes)
	
	For q As Integer = clist.Count-1 To 0 Step -1
		
		'remove private constructors
		'if no public constructor available remove class
		Dim As Integer totalCtorCnt = clist[q].ctors.Count
		Dim As Integer privateCtorCnt = 0
		For w As Integer = clist[q].ctors.Count-1 To 0 Step -1
			If clist[q].ctors[w].acc = CX_CXXPrivate Then
				privateCtorCnt += 1
				clist[q].ctors.RemoveAt(w)
			EndIf
		Next
		If totalCtorCnt > 0 AndAlso totalCtorCnt = privateCtorCnt Then clist.RemoveAt(q) : Continue For
		
		
		prepare_Class(clist[q])
		
		prepare_global(clist[q].innerClasses)
		
	Next
End Sub







Function render_Class(ByRef c As TClass) As String
	
	
	Dim As String res
	'Header
	res += "/*****************************************************************" + NL
	res += "  class " + c.nam + NL
	res += " *****************************************************************/" + NL
	
	If c.inheritable Then
		c.vnam = "Wrapper_" + c.nam
		'Callbacks Typedefs
		If c.dtor.vOverride Then
			res += "typedef void (CALLBACK* " + c.dtor.vtyp + ") ();" + NL
		EndIf
		Dim As TClassVMethodIterator it = TClassVMethodIterator(@c)
		While it.MoveNext
			Dim m As TMethod Ptr = @it.Current
			
			res += "typedef " + m->ret + " (CALLBACK* " + m->vtyp + ") ("  
			For w As Integer = 0 To m->params.Count-1
				If w > 0 Then res += ", "
				res += m->params[w].typ + " " + m->params[w].nam
			Next
			res += ");" + NL
		Wend
		
		'Wrapper class
		res += "class " + c.vnam + " : public " + c.nam + " {" + NL
		
		'Callback member vars
		res += "private:" + NL
		If c.dtor.vOverride Then
			res += "  " + c.dtor.vtyp + " " + c.dtor.vnam + ";" + NL
		EndIf
		it.Reset_
		While it.MoveNext
			Dim m As TMethod Ptr = @it.Current
			res += "  " + m->vtyp + " " + m->vnam + ";" + NL
		Wend
		
		'Constructor(s)
		res += "public:" + NL
		For q As Integer = 0 To c.ctors.Count-1
			res += "  " + c.vnam + "("
			Dim paramStr As String
			For w As Integer = 0 To c.ctors[q].params.Count-1
				If w > 0 Then res += ", " : paramStr += ", "
				res += c.ctors[q].params[w].typ + " " + c.ctors[q].params[w].nam
				paramStr += c.ctors[q].params[w].nam
			Next
			res += ") : " + c.nam + "(" + paramStr + ")"
			If c.dtor.vOverride Then
				res += ", " + c.dtor.vnam + "(0)"
			EndIf
			it.Reset_
			While it.MoveNext
				Dim m As TMethod Ptr = @it.Current
				res += ", " + m->vnam + "(0)"
			Wend
			res += " { }" + NL
		Next
		
		'Destructor
		If c.dtor.vnam <> "" Then
			res += "  virtual ~" + c.vnam + "() {" + NL
			res += "    if (" + c.dtor.vnam + ") " + c.dtor.vnam + "();" + NL
			res += "  }" + NL
		EndIf
		
		'RegisterVirtual method
		Scope
			res += "  void RegisterVirtual("
			Dim As String regVirtBody
			Dim As Integer needComma = 0
			If c.dtor.vOverride Then
				res += c.dtor.vtyp + " " + c.dtor.vnam
				regVirtBody += "    this->" + c.dtor.vnam + "=" + c.dtor.vnam + ";" + NL
				needComma = 1
			EndIf
			it.Reset_
			While it.MoveNext
				Dim m As TMethod Ptr = @it.Current
				If needComma Then res += ", " Else needComma = 1
				res += m->vtyp + " " + m->vnam
				regVirtBody += "    this->" + m->vnam + "=" + m->vnam + ";" + NL
			Wend
			res += ") {" + NL
			res += regVirtBody
			res += "  }" + NL
		End Scope
		
		'UnregisterVirtual method
		res += "  void UnregisterVirtual() {" + NL
		If c.dtor.vOverride Then
			res += "    this->" + c.dtor.vnam + "=" + c.dtor.vnam + ";" + NL
		EndIf
		it.Reset_
		While it.MoveNext
			Dim m As TMethod Ptr = @it.Current
			res += "    this->" + m->vnam + "=0;" + NL
		Wend
		res += "  }" + NL
		
		'Virtual methods
		it.Reset_
		While it.MoveNext
			Dim m As TMethod Ptr = @it.Current
			res += "  virtual " + m->ret + " " + m->nam + "("
			Dim As String virtCallParams
			For w As Integer = 0 To m->params.Count-1
				If w > 0 Then res += ", ": virtCallParams += ", "
				res += m->params[w].typ + " " + m->params[w].nam
				virtCallParams += m->params[w].nam
			Next
			res += ") "
			If m->isConst Then res += "const "
			res += "{" + NL
			res += "    if (" + m->vnam + ") "
			If m->ret <> "void" Then res += "return "
			res += m->vnam + "(" + virtCallParams + ");" + NL
			res += "    return " + c.nam + "::" + m->nam + "(" + virtCallParams + ");" + NL
			res += "  }" + NL
		Wend
		
		'End of Wrapper class
		res += "};" + NL
		
		
		'Export: RegisterVirtual
		Scope
			res += "WXFB_EXPORT(void)" + NL
			res += "  " + c.nam + "_RegisterVirtual (" + c.vnam + "* self"
			Dim As String regVirtCallParams
			Dim As Integer needComma = 0
			If c.dtor.vOverride Then
				res += ", " + c.dtor.vtyp + " " + c.dtor.vnam
				regVirtCallParams += c.dtor.vnam
				needComma = 1
			EndIf
			it.Reset_
			While it.MoveNext
				Dim m As TMethod Ptr = @it.Current
				If needComma Then regVirtCallParams += ", " Else needComma = 1
				res += ", " + m->vtyp + " " + m->vnam
				regVirtCallParams += m->vnam
			Wend
			res += ") {" + NL
			res += "    if (self) self->RegisterVirtual(" + regVirtCallParams + ");" + NL
			res += "  }" + NL
		End Scope
		
		
		'Export: UnregisterVirtual
		res += "WXFB_EXPORT(void)" + NL
		res += "  " + c.nam + "_UnregisterVirtual (" + c.vnam + "* self) {" + NL
		res += "    if (self) self->UnregisterVirtual();" + NL
		res += "  }" + NL
		
	EndIf
	
	
	
	'Export: Constructor(s)
	For q As Integer = 0 To c.ctors.Count-1
		Dim m As TMethod Ptr = @c.ctors[q]
		res += "WXFB_EXPORT(" + c.nam + "*)" + NL
		res += "  " + m->xnam + "("
		Dim paramStr As String
		For w As Integer = 0 To m->params.Count-1
			If w > 0 Then res += ", " : paramStr += ", "
			res += m->params[w].xtyp + " " + m->params[w].nam
			paramStr += m->params[w].nam
		Next
		res += ") {" + NL
		res += "    return WXFB_NEW(" + IIf(c.inheritable, c.vnam, c.nam) + ", (" + paramStr + ") );" + NL
		res += "  }" + NL
	Next
	
	'Export: Destructor
	res += "WXFB_EXPORT(void)" + NL
	res += "  " + c.dtor.xnam + "(" + IIf(c.inheritable, c.vnam, c.nam) + "* self) {" + NL
	res += "    WXFB_DEL(self);" + NL
	res += "  }" + NL
	
	'Export Methods
	For q As Integer = 0 To c.methods.Count-1
		Dim m As TMethod Ptr = @c.methods[q]
		If (m->acc = CX_CXXPublic OrElse c.inheritable) Then
			'Dim tk As Integer = clang_getCanonicalType(clang_getCursorResultType(m->cur)).kind
			'If tk=CXType_Void OrElse tk=CXType_Bool OrElse tk=CXType_Char_U OrElse tk=CXType_UChar OrElse tk=CXType_Char16 OrElse tk=CXType_Char32 OrElse tk=CXType_UShort OrElse tk=CXType_UInt OrElse tk=CXType_ULong OrElse tk=CXType_WChar OrElse tk=CXType_Short OrElse tk=CXType_Int OrElse tk=CXType_Long OrElse tk=CXType_Float OrElse tk=CXType_Double OrElse tk=CXType_NullPtr OrElse tk=CXType_Pointer OrElse tk=CXType_Enum Then
			'	res += "/* returns: " + getCXS(clang_getTypeKindSpelling( clang_getCanonicalType(clang_getCursorResultType(m->cur)).kind ))  + "*/" + NL
			'Else
			'	res += "/* ######## WARNING ########    returns: " + getCXS(clang_getTypeKindSpelling( clang_getCanonicalType(clang_getCursorResultType(m->cur)).kind ))  + "*/" + NL
			'EndIf
			res += "WXFB_EXPORT(" + m->xret + ")" + NL
			res += "  " + c.nam + "_" + m->xnam + "("
			If m->inh <> CX_Static Then
				If m->vOverride Then res += IIf(c.inheritable, c.vnam, c.nam) Else res += c.nam
				res += "* self"
			EndIf
			Dim paramStr As String
			For w As Integer = 0 To m->params.Count-1
				If w > 0 Then paramStr += ", "
				If w > 0 OrElse m->inh <> CX_Static Then res += ", "
				res += m->params[w].xtyp + " " + m->params[w].nam
				paramStr += m->params[w].nam
			Next
			res += ") {" + NL
			res += "    "
			If m->xret <> "void" Then res += "return "
			If m->inh <> CX_Static Then res += "self->" Else res += IIf(c.inheritable, c.vnam, c.nam) + "::"
			res += m->nam + "(" + paramStr + ");" + NL
			res += "  }" + NL
		EndIf
	Next
	
	
	Return res
End Function




Function render_Type(ByRef c As TClass) As String
	
	Dim As String intObj = "internal_obj"
	Dim As String intNS = "wxInternal"


	c.pnam = "internal_" + c.nam
	
	Dim As String res
	
	'Header
	res += "' class " + c.nam + NL
	
	res += "Namespace " + intNS + NL
	
	If c.inheritable Then
		
		'Class pointer
		res += "  Type " + c.pnam + " As Any" + NL
		
		'Callbacks Typedefs
		If c.dtor.vOverride Then
			res += "  Type " + c.dtor.vtyp + " As Sub WXCALL ()" + NL
		EndIf
		For q As Integer = 0 To c.methods.Count-1
			If c.methods[q].vOverride Then
				Dim m As TMethod Ptr = @c.methods[q]
				res += "  Type " + m->vtyp + " As "  
				If m->ret = "void" Then res += "Sub WXCALL (" Else res += "Function WXCALL ("
				For w As Integer = 0 To m->params.Count-1
					If w > 0 Then res += ", "
					res += m->params[w].nam + " As " + m->params[w].typ
				Next
				res += ") As " + m->ret + NL
			EndIf
		Next
		
		
	EndIf
	
	
	'Import: Constructor(s)
	For q As Integer = 0 To c.ctors.Count-1
		Dim m As TMethod Ptr = @c.ctors[q]
		res += "  Declare Function " + m->xnam + " WXCALL Alias """ + m->xnam + """ ("
		For w As Integer = 0 To m->params.Count-1
			If w > 0 Then res += ", "
			res += m->params[w].nam + " As " + m->params[w].typ
		Next
		res += ") As " + c.pnam + " Ptr" + NL
	Next
	
	'Import: Destructor
	res += "  Declare Sub " + c.dtor.xnam + " WXCALL Alias """ + c.dtor.xnam + """ (self As " + c.pnam + " Ptr)" + NL
	
	
	
	If c.inheritable Then
	
		'Import: RegisterVirtual
		res += "  Declare Sub " + c.nam + "_RegisterVirtual WXCALL Alias """ + c.nam + "_RegisterVirtual"" (self As " + c.pnam + " Ptr"
		If c.dtor.vOverride Then
			res += ", " + c.dtor.vnam + " As " + c.dtor.vtyp
		EndIf
		For q As Integer = 0 To c.methods.Count-1
			If c.methods[q].vOverride Then
				Dim m As TMethod Ptr = @c.methods[q]
				res += ", " + m->vnam + " As " + m->vtyp
			EndIf
		Next
		res += ")" + NL
		
		'Export: UnregisterVirtual
		res += "  Declare Sub " + c.nam + "_UnregisterVirtual WXCALL Alias """ + c.nam + "_UnregisterVirtual"" (self As " + c.pnam + " Ptr)" + NL
		
	EndIf
	
	
	'Export Methods
	For q As Integer = 0 To c.methods.Count-1
		Dim m As TMethod Ptr = @c.methods[q]
		res += "  Declare "
		If m->ret = "void" Then res += "Sub " Else res += "Function "
		res += c.nam + "_" + m->xnam + " WXCALL Alias """ + c.nam + "_" + m->xnam + """ ("
		If m->inh <> CX_Static Then res += "self As " + c.pnam + " Ptr"
		For w As Integer = 0 To m->params.Count-1
			If w > 0 OrElse m->inh <> CX_Static Then res += ", "
			res += m->params[w].nam + " As " + m->params[w].typ
			If m->params[w].defVal <> "" Then res += " = " + m->params[w].defVal
		Next
		If m->ret = "void" Then res += ") As " + m->ret + NL Else res += ")" + NL
	Next
	
	
	
	res += "End Namespace " + intNS + NL
	
	
	
	res += "Type " + c.nam + NL
	
	res += "private:" + NL
	res += "  " + intObj + " As " + intNS + "." + c.pnam + " Ptr" + NL
	
	res += "public:" + NL
	
	
	res += "End Type"
	
	
	Return res
	
End Function









Sub printClasses(clist As stwListClass Ptr = @Classes, level As Integer = 0)
	
	For q As Integer = 0 To clist->Count-1
		prepare_Class((*clist)[q])
		render_Class((*clist)[q])
		render_Type((*clist)[q])
		'Class name + base classes
		Print space(level*2); (*clist)[q].ns; "::"; (*clist)[q].nam;
		Print "  extends  ";
		For w As Integer = 0 To (*clist)[q].baseClassesStr.Count-1
			Print (*clist)[q].baseClassesStr[w]; "  ";
		Next
		Print
		'Class fields
		For w As Integer = 0 To (*clist)[q].fields.Count-1
			Print space(level*2); " -"; (*clist)[q].fields[w].nam
		Next
		'Class methods
		For w As Integer = 0 To (*clist)[q].methods.Count-1
			Print space(level*2); " +"; (*clist)[q].methods[w].nam; "(";
			For e As Integer = 0 To (*clist)[q].methods[w].params.Count-1
				Print (*clist)[q].methods[w].params[e].typ; " "; (*clist)[q].methods[w].params[e].nam; "="; (*clist)[q].methods[w].params[e].defVal; ", ";
			Next
			Print ") returns "; (*clist)[q].methods[w].ret
		Next
		printClasses(@((*clist)[q].innerClasses), level+1)
		'enums
		For w As Integer = 0 To (*clist)[q].enums.Count-1
			Print space(level*2); " *"; (*clist)[q].enums[w].nam; "{";
			For e As Integer = 0 To (*clist)[q].enums[w].values.Count-1
				Print (*clist)[q].enums[w].values[e].nam; "="; (*clist)[q].enums[w].values[e].value; ", ";
			Next
			Print "}"
		Next
	Next
End Sub



Print
Print "Classes:"
'printClasses()





Sub writeOutput()
	
	If Dir("wxfb.bi") <> "" Then Kill "wxfb.bi"
	
	Open "wxfb.bi" For Binary As #2
	
	prepare_global()
	
	For q As Integer = 0 To Classes.Count-1
		If Dir("E:\qt\test\wxfb\gen\"+Classes[q].nam + ".cpp") <> "" Then Kill "E:\qt\test\wxfb\gen\"+Classes[q].nam + ".cpp"
		Open "E:\qt\test\wxfb\gen\"+Classes[q].nam + ".cpp" For Binary As #1
		Put #1,,"#include ""wxfb_globals.h""" + NL+NL
		Put #1,,render_Class(Classes[q])
		Put #1,,NL+NL+NL
		Put #2,,render_Type(Classes[q])
		Put #2,,NL+NL+NL
		
		Close #1
	Next
	
	Close #2
	
End Sub

writeOutput()







clang_disposeTranslationUnit(cxtu)
clang_disposeIndex(cidx)


Print "- end -"

Sleep




Function getCursorKindStr(kind As CXCursorKind) As String
Select Case kind
Case CXCursor_UnexposedDecl: return "CXCursor_UnexposedDecl"
case CXCursor_StructDecl: return "CXCursor_StructDecl"
case CXCursor_UnionDecl: return "CXCursor_UnionDecl"
case CXCursor_ClassDecl: return "CXCursor_ClassDecl"
case CXCursor_EnumDecl: return "CXCursor_EnumDecl"
case CXCursor_FieldDecl: return "CXCursor_FieldDecl"
case CXCursor_EnumConstantDecl: return "CXCursor_EnumConstantDecl"
case CXCursor_FunctionDecl: return "CXCursor_FunctionDecl"
case CXCursor_VarDecl: return "CXCursor_VarDecl"
case CXCursor_ParmDecl: return "CXCursor_ParmDecl"
case CXCursor_ObjCInterfaceDecl: return "CXCursor_ObjCInterfaceDecl"
case CXCursor_ObjCCategoryDecl: return "CXCursor_ObjCCategoryDecl"
case CXCursor_ObjCProtocolDecl: return "CXCursor_ObjCProtocolDecl"
case CXCursor_ObjCPropertyDecl: return "CXCursor_ObjCPropertyDecl"
case CXCursor_ObjCIvarDecl: return "CXCursor_ObjCIvarDecl"
case CXCursor_ObjCInstanceMethodDecl: return "CXCursor_ObjCInstanceMethodDecl"
case CXCursor_ObjCClassMethodDecl: return "CXCursor_ObjCClassMethodDecl"
case CXCursor_ObjCImplementationDecl: return "CXCursor_ObjCImplementationDecl"
case CXCursor_ObjCCategoryImplDecl: return "CXCursor_ObjCCategoryImplDecl"
case CXCursor_TypedefDecl: return "CXCursor_TypedefDecl"
case CXCursor_CXXMethod: return "CXCursor_CXXMethod"
case CXCursor_Namespace: return "CXCursor_Namespace"
case CXCursor_LinkageSpec: return "CXCursor_LinkageSpec"
case CXCursor_Constructor: return "CXCursor_Constructor"
case CXCursor_Destructor: return "CXCursor_Destructor"
case CXCursor_ConversionFunction: return "CXCursor_ConversionFunction"
case CXCursor_TemplateTypeParameter: return "CXCursor_TemplateTypeParameter"
case CXCursor_NonTypeTemplateParameter: return "CXCursor_NonTypeTemplateParameter"
case CXCursor_TemplateTemplateParameter: return "CXCursor_TemplateTemplateParameter"
case CXCursor_FunctionTemplate: return "CXCursor_FunctionTemplate"
case CXCursor_ClassTemplate: return "CXCursor_ClassTemplate"
case CXCursor_ClassTemplatePartialSpecialization: return "CXCursor_ClassTemplatePartialSpecialization"
case CXCursor_NamespaceAlias: return "CXCursor_NamespaceAlias"
case CXCursor_UsingDirective: return "CXCursor_UsingDirective"
case CXCursor_UsingDeclaration: return "CXCursor_UsingDeclaration"
case CXCursor_TypeAliasDecl: return "CXCursor_TypeAliasDecl"
case CXCursor_ObjCSynthesizeDecl: return "CXCursor_ObjCSynthesizeDecl"
case CXCursor_ObjCDynamicDecl: return "CXCursor_ObjCDynamicDecl"
case CXCursor_CXXAccessSpecifier: return "CXCursor_CXXAccessSpecifier"
case CXCursor_FirstDecl: return "CXCursor_FirstDecl"
case CXCursor_LastDecl: return "CXCursor_LastDecl"
case CXCursor_FirstRef: return "CXCursor_FirstRef"
case CXCursor_ObjCSuperClassRef: return "CXCursor_ObjCSuperClassRef"
case CXCursor_ObjCProtocolRef: return "CXCursor_ObjCProtocolRef"
case CXCursor_ObjCClassRef: return "CXCursor_ObjCClassRef"
case CXCursor_TypeRef: return "CXCursor_TypeRef"
case CXCursor_CXXBaseSpecifier: return "CXCursor_CXXBaseSpecifier"
case CXCursor_TemplateRef: return "CXCursor_TemplateRef"
case CXCursor_NamespaceRef: return "CXCursor_NamespaceRef"
case CXCursor_MemberRef: return "CXCursor_MemberRef"
case CXCursor_LabelRef: return "CXCursor_LabelRef"
case CXCursor_OverloadedDeclRef: return "CXCursor_OverloadedDeclRef"
case CXCursor_VariableRef: return "CXCursor_VariableRef"
case CXCursor_LastRef: return "CXCursor_LastRef"
case CXCursor_FirstInvalid: return "CXCursor_FirstInvalid"
case CXCursor_InvalidFile: return "CXCursor_InvalidFile"
case CXCursor_NoDeclFound: return "CXCursor_NoDeclFound"
case CXCursor_NotImplemented: return "CXCursor_NotImplemented"
case CXCursor_InvalidCode: return "CXCursor_InvalidCode"
case CXCursor_LastInvalid: return "CXCursor_LastInvalid"
case CXCursor_FirstExpr: return "CXCursor_FirstExpr"
case CXCursor_UnexposedExpr: return "CXCursor_UnexposedExpr"
case CXCursor_DeclRefExpr: return "CXCursor_DeclRefExpr"
case CXCursor_MemberRefExpr: return "CXCursor_MemberRefExpr"
case CXCursor_CallExpr: return "CXCursor_CallExpr"
case CXCursor_ObjCMessageExpr: return "CXCursor_ObjCMessageExpr"
case CXCursor_BlockExpr: return "CXCursor_BlockExpr"
case CXCursor_IntegerLiteral: return "CXCursor_IntegerLiteral"
case CXCursor_FloatingLiteral: return "CXCursor_FloatingLiteral"
case CXCursor_ImaginaryLiteral: return "CXCursor_ImaginaryLiteral"
case CXCursor_StringLiteral: return "CXCursor_StringLiteral"
case CXCursor_CharacterLiteral: return "CXCursor_CharacterLiteral"
case CXCursor_ParenExpr: return "CXCursor_ParenExpr"
case CXCursor_UnaryOperator: return "CXCursor_UnaryOperator"
case CXCursor_ArraySubscriptExpr: return "CXCursor_ArraySubscriptExpr"
case CXCursor_BinaryOperator: return "CXCursor_BinaryOperator"
case CXCursor_CompoundAssignOperator: return "CXCursor_CompoundAssignOperator"
case CXCursor_ConditionalOperator: return "CXCursor_ConditionalOperator"
case CXCursor_CStyleCastExpr: return "CXCursor_CStyleCastExpr"
case CXCursor_CompoundLiteralExpr: return "CXCursor_CompoundLiteralExpr"
case CXCursor_InitListExpr: return "CXCursor_InitListExpr"
case CXCursor_AddrLabelExpr: return "CXCursor_AddrLabelExpr"
case CXCursor_StmtExpr: return "CXCursor_StmtExpr"
case CXCursor_GenericSelectionExpr: return "CXCursor_GenericSelectionExpr"
case CXCursor_GNUNullExpr: return "CXCursor_GNUNullExpr"
case CXCursor_CXXStaticCastExpr: return "CXCursor_CXXStaticCastExpr"
case CXCursor_CXXDynamicCastExpr: return "CXCursor_CXXDynamicCastExpr"
case CXCursor_CXXReinterpretCastExpr: return "CXCursor_CXXReinterpretCastExpr"
case CXCursor_CXXConstCastExpr: return "CXCursor_CXXConstCastExpr"
case CXCursor_CXXFunctionalCastExpr: return "CXCursor_CXXFunctionalCastExpr"
case CXCursor_CXXTypeidExpr: return "CXCursor_CXXTypeidExpr"
case CXCursor_CXXBoolLiteralExpr: return "CXCursor_CXXBoolLiteralExpr"
case CXCursor_CXXNullPtrLiteralExpr: return "CXCursor_CXXNullPtrLiteralExpr"
case CXCursor_CXXThisExpr: return "CXCursor_CXXThisExpr"
case CXCursor_CXXThrowExpr: return "CXCursor_CXXThrowExpr"
case CXCursor_CXXNewExpr: return "CXCursor_CXXNewExpr"
case CXCursor_CXXDeleteExpr: return "CXCursor_CXXDeleteExpr"
case CXCursor_UnaryExpr: return "CXCursor_UnaryExpr"
case CXCursor_ObjCStringLiteral: return "CXCursor_ObjCStringLiteral"
case CXCursor_ObjCEncodeExpr: return "CXCursor_ObjCEncodeExpr"
case CXCursor_ObjCSelectorExpr: return "CXCursor_ObjCSelectorExpr"
case CXCursor_ObjCProtocolExpr: return "CXCursor_ObjCProtocolExpr"
case CXCursor_ObjCBridgedCastExpr: return "CXCursor_ObjCBridgedCastExpr"
case CXCursor_PackExpansionExpr: return "CXCursor_PackExpansionExpr"
case CXCursor_SizeOfPackExpr: return "CXCursor_SizeOfPackExpr"
case CXCursor_LambdaExpr: return "CXCursor_LambdaExpr"
case CXCursor_ObjCBoolLiteralExpr: return "CXCursor_ObjCBoolLiteralExpr"
case CXCursor_ObjCSelfExpr: return "CXCursor_ObjCSelfExpr"
case CXCursor_LastExpr: return "CXCursor_LastExpr"
case CXCursor_FirstStmt: return "CXCursor_FirstStmt"
case CXCursor_UnexposedStmt: return "CXCursor_UnexposedStmt"
case CXCursor_LabelStmt: return "CXCursor_LabelStmt"
case CXCursor_CompoundStmt: return "CXCursor_CompoundStmt"
case CXCursor_CaseStmt: return "CXCursor_CaseStmt"
case CXCursor_DefaultStmt: return "CXCursor_DefaultStmt"
case CXCursor_IfStmt: return "CXCursor_IfStmt"
case CXCursor_SwitchStmt: return "CXCursor_SwitchStmt"
case CXCursor_WhileStmt: return "CXCursor_WhileStmt"
case CXCursor_DoStmt: return "CXCursor_DoStmt"
case CXCursor_ForStmt: return "CXCursor_ForStmt"
case CXCursor_GotoStmt: return "CXCursor_GotoStmt"
case CXCursor_IndirectGotoStmt: return "CXCursor_IndirectGotoStmt"
case CXCursor_ContinueStmt: return "CXCursor_ContinueStmt"
case CXCursor_BreakStmt: return "CXCursor_BreakStmt"
case CXCursor_ReturnStmt: return "CXCursor_ReturnStmt"
case CXCursor_GCCAsmStmt: return "CXCursor_GCCAsmStmt"
case CXCursor_AsmStmt: return "CXCursor_AsmStmt"
case CXCursor_ObjCAtTryStmt: return "CXCursor_ObjCAtTryStmt"
case CXCursor_ObjCAtCatchStmt: return "CXCursor_ObjCAtCatchStmt"
case CXCursor_ObjCAtFinallyStmt: return "CXCursor_ObjCAtFinallyStmt"
case CXCursor_ObjCAtThrowStmt: return "CXCursor_ObjCAtThrowStmt"
case CXCursor_ObjCAtSynchronizedStmt: return "CXCursor_ObjCAtSynchronizedStmt"
case CXCursor_ObjCAutoreleasePoolStmt: return "CXCursor_ObjCAutoreleasePoolStmt"
case CXCursor_ObjCForCollectionStmt: return "CXCursor_ObjCForCollectionStmt"
case CXCursor_CXXCatchStmt: return "CXCursor_CXXCatchStmt"
case CXCursor_CXXTryStmt: return "CXCursor_CXXTryStmt"
case CXCursor_CXXForRangeStmt: return "CXCursor_CXXForRangeStmt"
case CXCursor_SEHTryStmt: return "CXCursor_SEHTryStmt"
case CXCursor_SEHExceptStmt: return "CXCursor_SEHExceptStmt"
case CXCursor_SEHFinallyStmt: return "CXCursor_SEHFinallyStmt"
case CXCursor_MSAsmStmt: return "CXCursor_MSAsmStmt"
case CXCursor_NullStmt: return "CXCursor_NullStmt"
case CXCursor_DeclStmt: return "CXCursor_DeclStmt"
case CXCursor_LastStmt: return "CXCursor_LastStmt"
case CXCursor_TranslationUnit: return "CXCursor_TranslationUnit"
case CXCursor_FirstAttr: return "CXCursor_FirstAttr"
case CXCursor_UnexposedAttr: return "CXCursor_UnexposedAttr"
case CXCursor_IBActionAttr: return "CXCursor_IBActionAttr"
case CXCursor_IBOutletAttr: return "CXCursor_IBOutletAttr"
case CXCursor_IBOutletCollectionAttr: return "CXCursor_IBOutletCollectionAttr"
case CXCursor_CXXFinalAttr: return "CXCursor_CXXFinalAttr"
case CXCursor_CXXOverrideAttr: return "CXCursor_CXXOverrideAttr"
case CXCursor_AnnotateAttr: return "CXCursor_AnnotateAttr"
case CXCursor_AsmLabelAttr: return "CXCursor_AsmLabelAttr"
case CXCursor_LastAttr: return "CXCursor_LastAttr"
case CXCursor_PreprocessingDirective: return "CXCursor_PreprocessingDirective"
case CXCursor_MacroDefinition: return "CXCursor_MacroDefinition"
case CXCursor_MacroExpansion: return "CXCursor_MacroExpansion"
case CXCursor_MacroInstantiation: return "CXCursor_MacroInstantiation"
case CXCursor_InclusionDirective: return "CXCursor_InclusionDirective"
case CXCursor_FirstPreprocessing: return "CXCursor_FirstPreprocessing"
case CXCursor_LastPreprocessing: return "CXCursor_LastPreprocessing"
case CXCursor_ModuleImportDecl: return "CXCursor_ModuleImportDecl"
case CXCursor_FirstExtraDecl: return "CXCursor_FirstExtraDecl"
case CXCursor_LastExtraDecl: return "CXCursor_LastExtraDecl"
End Select
End Function



Function getAccessSpecifierStr(accSpec As CX_CXXAccessSpecifier) As String
	Select Case accSpec
		Case CX_CXXInvalidAccessSpecifier: Return "CX_CXXInvalidAccessSpecifier"
		Case CX_CXXPublic: Return "CX_CXXPublic"
		Case CX_CXXProtected: Return "CX_CXXProtected"
		Case CX_CXXPrivate: Return "CX_CXXPrivate"
	End Select
End Function

Function clean_path(filepath As String) As String
	For q As Integer = 1 To Len(filepath)
		If filepath[q-1] = asc("/") Then filepath[q-1] = Asc("\")
	Next
	Return LCase(filepath)
End Function
