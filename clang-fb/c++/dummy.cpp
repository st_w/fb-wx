
#define Integer int
#define DEFINE_METHODS                                      \
    int macro_meth1(int a) {return 0;}                      \
    int macro_meth2(int a) {return 1;}                      \
    int macro_variable = 1;





class Person {
};


class Room {
public:
    void add_person(Person person)
    {
        // do stuff
    }

private:
    Person* people_in_room;
};


template <class T, int N>
class Bag<T, N> {
};


int main()
{
    Person* p = new Person();
    Bag<Person, 42> bagofpersons;

    return 0;
}


namespace a {
namespace b {
class c {
	class e;
	private: class p_a;
	public: class p_b;
};
}
class d ;
}


int test_function(int firstparam, Person secondparam[][2] , int*** thirdparam, Person* fourthParam[]) {
	return 0;
}

DEFINE_METHODS

int test_function2(Integer hallo) {
	return 0;
}


#include "fake-class.h"
#include "fake-class-protected.h"
