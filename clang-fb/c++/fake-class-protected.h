
//#pragma once

#ifndef protection
#define protection

class OpaqueClass;

struct FakeClass {
    int a;
    OpaqueClass *p;
    FakeClass();
};

#endif