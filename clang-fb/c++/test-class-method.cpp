#define WXUNUSED(identifier)


class abstractClass {
	virtual int virt();
	virtual int pureVirt() = 0;
    virtual ~abstractClass();
	int noAccMeth();
	private:
	int privateMeth();
	protected:
	int protectedMeth();
	public:
	int publicMeth();
    int constMeth() const;
};


class concreteClass {
	int noAccVar;
	~concreteClass();
	concreteClass();
	concreteClass(int dummy);
	private:
	int privateVar;
	protected:
	int protectedVar;
	public:
	int publicVar;
    enum Enum { ENUM_A=1+2, ENUM_B, ENUM_C };
    void defParamMeth(int a = 12, double b = 3.0, int c = 3+3, Enum d = ENUM_B);
    static int statMeth(int a);
    static int statMeth(int a, int b, int c = 2);
    virtual int virtMeth(int a);
    virtual int virtMeth(int a, int b);
    virtual int virtMeth(int&a, int &b, float& c, bool & d);
    int missingParam(int WXUNUSED(a), int WXUNUSED(b));
    int operator+(int const& rhs) const;
    int operator+(float const& rhs) const;
    int operator+(double const& rhs) const;
    int operator   -   (int const& rhs) const;
    operator double() const { return 1.0; }     //conversion operator; CXCursor_ConversionFunction
};


class publicInhClass : public abstractClass {
	
};
class protectedInhClass : protected abstractClass {
	
};
class privateInhClass : private abstractClass {
	
};
class implInhClass : public abstractClass {
	virtual int pureVirt() {
		return 0;
	}
};
class multiInhClass : public abstractClass, public concreteClass {
	
};






namespace Persons {

	class Eddie {
	
		int m_name;

		private:

		void die() {
		}

		public:
		
		int age;

		int go(int x, int y) {
		}

		static Eddie* oneself() {
			return 0;
		}


		class EddieEye {
			int color;
		};

	};
	
	
	
	namespace Pets {
	
		class Dog {
		};
		
		
		class LittleDog : Dog {
		};
	
	}
	
	

}









