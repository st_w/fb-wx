﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace interface_parser.Lexer
{
    public enum TokenType
    {
        Semicolon,      // ;
        Colon,          // :
        Dot,            // .
        Comma,          // ,
        Assign,         // =
        Less,           // <
        Greater,        // >
        Tilde,          // ~
        LBrace,         // {
        RBrace,         // }
        LBracket,       // [
        RBracket,       // ]
        LPara,          // (
        RPara,          // )
        Amp,            // &
        Excl,           // !
        Quest,          // ?

        Incr,           // ++
        Decr,           // --
        LShift,         // <<
        RShift,         // >>

        Arrow,          // ->
        ArrowStar,      // ->*
        DotStar,        // .*
        NS,             // ::
        Ellipsis,       // ...

        Class,
        Typedef,
        Operator,
        New,
        NewArr,         // new[]
        Delete,
        DeleteArr,      // delete[]
        Template,
        Inline,

        Public,
        Protected,
        Private,
        Abstract,
        Virtual,
        Static,

        Const,
        Volatile,
        Union,
        Struct,
        Enum,

        Void,
        Int,
        Char,
        Long,
        Float,
        Double,
        WChar,          // wchar_t
        Signed,
        Unsigned,

        Plus,           // +
        Minus,          // -
        Star,           // *
        Div,            // /
        Mod,            // %
        Exp,            // ^

        PlusEqu,        // +=
        MinusEqu,       // -=
        MulEqu,         // *=
        DivEqu,         // /=
        ModEqu,         // %=
        LShiftEqu,      // <<=
        RShiftEqu,      // >>=
        ExpEqu,         // ^=
        OrEqu,          // |=
        AndEqu,         // &=

        LessEqu,        // <=
        GreaterEqu,     // >=
        Equals,         // ==
        NEqu,           // !=

        Or,             // |
        OrElse,         // ||
        AndAlso,        // &&

        Literal,
        Identifier,

        Unknown,
        Empty
    }

    public class Token
    {
        public TokenPosition Position { get; private set; }
        public TokenType Type { get; private set; }
        public object Value { get; private set; }
        public int IntValue { get { return Value as int? ?? 0; } }
        public string StrValue { get { return Value as string ?? ""; } }

        public Token(TokenType type, TokenPosition position, string value = "")
        {
            Type = type;
            Position = position;
            Value = value;
        }
        public Token(TokenType type, TokenPosition position, int value)
        {
            Type = type;
            Position = position;
            Value = value;
        }

        public override string ToString()
        {
            return Position.ToString() + " " + Type.ToString() + " " + Value.ToString();
        }

        public bool Is(System.Collections.Generic.IEnumerable<TokenType> tokens)
        {
            foreach (var tk in tokens)
            {
                if (tk == this.Type) return true;
            }
            return false;
        }
    }


    public class TokenPosition
    {
        public int Line { get; private set; }
        public int Column { get; private set; }

        public TokenPosition(int line, int column)
        {
            Line = line;
            Column = column;
        }

        public override string ToString()
        {
            return String.Format("[ln: {0}, col: {1}]", Line, Column); 
        }
    }




}
