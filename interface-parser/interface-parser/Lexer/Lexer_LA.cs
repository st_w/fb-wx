﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace interface_parser.Lexer
{
    class Lexer_LA : ILexer
    {
        private ILexer lex;
        private CircularBuffer<Token> buf;

        public int Line { get { return lex.Line; } }
        public int Column { get { return lex.Column; } }
        public TokenPosition Position { get { return lex.Position; } }

        public Lexer_LA(Lexer_SimpleC lex, int lookahead = 1)
        {
            this.lex = lex;
            buf = new CircularBuffer<Token>(lookahead);
            fillBuffer();
        }

        public Token Next()
        {
            Token t = buf.Dequeue();
            fillBuffer();
            return t;
        }

        public Token Peek(int la = 1)
        {
            return buf.Peek(la);
        }

        private void fillBuffer() {
            while (buf.Count < buf.Size) buf.Enqueue(lex.Next());
        }

    }
}
