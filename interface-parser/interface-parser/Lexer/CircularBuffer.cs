﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace interface_parser.Lexer
{

    /// <summary>
    /// Circular Buffer
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <see cref="http://stackoverflow.com/a/5924776"/>

    public class CircularBuffer<T> : IEnumerable<T>
    {
        readonly int size;

        int count;
        int head;
        int rear;
        T[] values;

        public CircularBuffer(int max)
        {
            this.size = max;
            count = 0;
            head = 0;
            rear = 0;
            values = new T[size];
        }

        static int Incr(int index, int size)
        {
            return (index + 1) % size;
        }

        private void EnsureQueueNotEmpty()
        {
            if (count == 0)
                throw new Exception("Empty queue");
        }

        public int Size { get { return size; } }

        public int Count { get { return count; } }

        public void Enqueue(T obj)
        {
            values[rear] = obj;

            if (Count == Size)
                head = Incr(head, Size);
            rear = Incr(rear, Size);
            count = Math.Min(count + 1, Size);
        }

        public T Dequeue()
        {
            EnsureQueueNotEmpty();

            T res = values[head];
            values[head] = default(T);
            head = Incr(head, Size);
            count--;

            return res;
        }

        public T Peek(int la = 1)
        {
            if (la < 1 || la > Count) throw new ArgumentException("Invalid Peek location");
            int pos = head;
            while (la > 1) pos = Incr(pos, Size);
            return values[pos];
        }



        public IEnumerator<T> GetEnumerator()
        {
            int index = head;
            for (int i = 0; i < count; i++)
            {
                yield return values[index];
                index = Incr(index, size);
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

    }
}
