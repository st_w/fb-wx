﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace interface_parser.Lexer
{
    class Lexer_SimpleC : ILexer
    {

        public int Line { get; private set; }
        public int Column { get; private set; }
        public TokenPosition Position { get { return new TokenPosition(Line, Column); } }

        StreamReader sr;
        char ch = ' ';

        Dictionary<string, TokenType> keywords = new Dictionary<string, TokenType>()
        {
            { "class", TokenType.Class },
            { "typedef", TokenType.Typedef },
            { "operator", TokenType.Operator },
            { "new", TokenType.New },
            { "new[]", TokenType.NewArr },
            { "delete", TokenType.Delete },
            { "delete[]", TokenType.DeleteArr },
            { "template", TokenType.Template },
            { "inline", TokenType.Inline },
            { "public", TokenType.Public },
            { "protected", TokenType.Protected },
            { "private", TokenType.Private },
            { "abstract", TokenType.Abstract },
            { "virtual", TokenType.Virtual },
            { "static", TokenType.Static },
            { "const", TokenType.Const },
            { "volatile", TokenType.Volatile },
            { "union", TokenType.Union },
            { "struct", TokenType.Struct },
            { "void", TokenType.Void },
            { "int", TokenType.Int },
            { "char", TokenType.Char },
            { "long", TokenType.Long },
            { "float", TokenType.Float },
            { "double", TokenType.Double },
            { "wchar_t", TokenType.WChar },
            { "signed", TokenType.Signed },
            { "unsigned", TokenType.Unsigned },
        };


        private void read(int cnt = 1)
        {
            Column++;
            while (cnt > 1) { sr.Read(); cnt--; }
            ch = (char)sr.Read();
        }


        public Token Next()
        {
            while (!sr.EndOfStream)
            {
                Token t = null;
                TokenPosition pos = Position;

                switch (ch)
                {
                    case '(': t = new Token(TokenType.LPara, pos); break;
                    case ')': t = new Token(TokenType.RPara, pos); break;
                    case '[': t = new Token(TokenType.LBracket, pos); break;
                    case ']': t = new Token(TokenType.RBracket, pos); break;
                    case '{': t = new Token(TokenType.LBrace, pos); break;
                    case '}': t = new Token(TokenType.RBrace, pos); break;
                    case ';': t = new Token(TokenType.Semicolon, pos); break;
                    case ',': t = new Token(TokenType.Comma, pos); break;
                    case '~': t = new Token(TokenType.Tilde, pos); break;
                    case '?': t = new Token(TokenType.Quest, pos); break;

                    case '+':
                        if (sr.Peek() == '+') { t = new Token(TokenType.Incr, pos); read(); }
                        else if (sr.Peek() == '=') { t = new Token(TokenType.PlusEqu, pos); read(); }
                        else { t = new Token(TokenType.Plus, pos); }
                        break;
                    case '-':
                        if (sr.Peek() == '-') { t = new Token(TokenType.Decr, pos); read(); }
                        else if (sr.Peek() == '=') { t = new Token(TokenType.MinusEqu, pos); read(); }
                        else if (sr.Peek() == '>')
                        {
                            read();
                            if (sr.Peek() == '*') { t = new Token(TokenType.ArrowStar, pos); read(); }
                            else { t = new Token(TokenType.Arrow, pos); }
                        }
                        else { t = new Token(TokenType.Minus, pos); }
                        break;
                    case '*':
                        if (sr.Peek() == '=') { t = new Token(TokenType.MulEqu, pos); read(); }
                        else { t = new Token(TokenType.Star, pos); }
                        break;
                    case '/':
                        if (sr.Peek() == '/' || sr.Peek() == '*') { }
                        else if (sr.Peek() == '=') { t = new Token(TokenType.DivEqu, pos); read(); }
                        else { t = new Token(TokenType.Div, pos); }
                        break;
                    case '%':
                        if (sr.Peek() == '=') { t = new Token(TokenType.ModEqu, pos); read(); }
                        else { t = new Token(TokenType.Mod, pos); }
                        break;
                    case '=':
                        if (sr.Peek() == '=') { t = new Token(TokenType.Equals, pos); read(); }
                        else { t = new Token(TokenType.Assign, pos); }
                        break;
                    case '<':
                        if (sr.Peek() == '<') {
                            read();
                            if (sr.Peek() == '=') { t = new Token(TokenType.LShiftEqu, pos); read(); }
                            else { t = new Token(TokenType.LShift, pos); }
                        }
                        else if (sr.Peek() == '=') { t = new Token(TokenType.LessEqu, pos); read(); }
                        else { t = new Token(TokenType.Less, pos); }
                        break;
                    case '>':
                        if (sr.Peek() == '>')
                        {
                            read();
                            if (sr.Peek() == '=') { t = new Token(TokenType.RShiftEqu, pos); read(); }
                            else { t = new Token(TokenType.RShift, pos); }
                        }
                        else if (sr.Peek() == '=') { t = new Token(TokenType.GreaterEqu, pos); read(); }
                        else { t = new Token(TokenType.Greater, pos); }
                        break;
                    case '!':
                        if (sr.Peek() == '=') { t = new Token(TokenType.NEqu, pos); read(); }
                        else { t = new Token(TokenType.Excl, pos); }
                        break;
                    case '&':
                        if (sr.Peek() == '&') { t = new Token(TokenType.AndAlso, pos); read(); }
                        else if (sr.Peek() == '=') { t = new Token(TokenType.AndEqu, pos); read(); }
                        else { t = new Token(TokenType.Amp, pos); }
                        break;
                    case '|':
                        if (sr.Peek() == '|') { t = new Token(TokenType.OrElse, pos); read(); }
                        else if (sr.Peek() == '=') { t = new Token(TokenType.OrEqu, pos); read(); }
                        else { t = new Token(TokenType.Or, pos); }
                        break;
                    case '^':
                        if (sr.Peek() == '=') { t = new Token(TokenType.ExpEqu, pos); read(); }
                        else { t = new Token(TokenType.Exp, pos); }
                        break;
                    case ':':
                        if (sr.Peek() == ':') { t = new Token(TokenType.NS, pos); read(); }
                        else { t = new Token(TokenType.Colon, pos); }
                        break;
                    case '.':
                        if (sr.Peek() == '.')
                        {
                            read(); read();
                            if (ch != '.') throw new Exception("Dot expected (found two dots instead of one/three)");
                            t = new Token(TokenType.Ellipsis, pos);
                        }
                        else if (sr.Peek() == '*') { t = new Token(TokenType.DotStar, pos); read(); }
                        else { t = new Token(TokenType.Dot, pos); }
                        break;

                }
                if (t != null)
                {
                    read();
                    return t;
                }

                // new line ( \r\n and \n supported)
                if (ch == '\n')
                {
                    read();
                    Column = 1;
                    Line++;
                }
                else if (ch == '\r' && sr.Peek() == '\n')
                {
                    read(2);
                    Column = 1;
                    Line++;
                }

                // whitespace: ignore
                else if (Char.IsWhiteSpace(ch))
                {
                    read();
                }

                // comments, slash
                else if (ch == '/')
                {
                    // Divide token
                    if (sr.Peek() != '/' && sr.Peek() != '*')
                    {
                        t = new Token(TokenType.Div, pos);
                        read();
                        return t;
                    }

                    read();

                    //single line comment
                    if (ch == '/')
                    {
                        read();
                        while (ch != '\n' && !sr.EndOfStream) { read(); }
                        read();
                        Column = 1;
                        Line++;
                    }
                    // multi line comment
                    else if (ch == '*')
                    {
                        read();
                        while (!(ch == '*' && sr.Peek() == '/') && !sr.EndOfStream)
                        {
                            if (ch == '\n') Line++;
                            read();
                        }
                        read(2);
                    }

                }

                // Identifier
                else if (Char.IsLetter(ch) || ch == '_')
                {
                    StringBuilder sb = new StringBuilder();
                    while ((Char.IsLetterOrDigit(ch) || ch == '_') && !sr.EndOfStream)
                    {
                        sb.Append(ch);
                        read();
                    }
                    string tk = sb.ToString();
                    if (keywords.ContainsKey(tk)) return new Token(keywords[tk], pos);
                    return new Token(TokenType.Identifier, pos, tk);
                }

                // Char Literal
                else if (ch == '\'')
                {
                    int val;
                    read();
                    if (ch == '\\') { val = parseCharLiteral(); }
                    else { val = ch; }
                    read();
                    //if (ch != '\'') throw new Exception("Expected end of literal");

                    return new Token(TokenType.Literal, pos, val);
                }

                // String Literal
                else if (ch == '"')
                {
                    StringBuilder sb = new StringBuilder();
                    read();
                    while (ch != '"' && !sr.EndOfStream)
                    {
                        sb.Append(ch);
                        read();
                        if (ch == '\\')
                        {
                            sb.Append(parseCharLiteral());
                        }
                    }
                    return new Token(TokenType.Literal, pos, sb.ToString());
                }

                // Number Literal
                else if (Char.IsDigit(ch))
                {
                    StringBuilder sb = new StringBuilder();
                    while (Char.IsDigit(ch) && !sr.EndOfStream)
                    {
                        sb.Append(ch);
                        read();
                    }
                    return new Token(TokenType.Literal, pos, Int32.Parse(sb.ToString()));
                }


                // no match
                else
                {
                    //throw new NotImplementedException();
                    t = new Token(TokenType.Unknown, pos);
                    read();
                    return t;
                }

                //read();

            }

            return new Token(TokenType.Empty, Position);
        }


        public Lexer_SimpleC(Stream inputStream)
        {
            if (inputStream == null) throw new ArgumentNullException("inputStream may not be null");
            sr = new StreamReader(inputStream);
            read();
            Line = 1;
        }


        private char parseCharLiteral()
        {
            char val;
            read();
            switch (ch)
            {
                case '0': val = '\0'; break;
                case 'a': val = '\a'; break;
                case 'b': val = '\b'; break;
                case 'f': val = '\f'; break;
                case 'n': val = '\n'; break;
                case 'r': val = '\r'; break;
                case 't': val = '\t'; break;
                case 'v': val = '\v'; break;
                case '\'': val = '\''; break;
                case '"': val = '"'; break;
                case '\\': val = '\\'; break;
                case '?': val = '?'; break;
                case 'x':
                    //2/4-digit hex number
                    throw new NotImplementedException();
                default:
                    //3-digit octal number
                    throw new NotImplementedException();
            }
            return val;
        }


        public Token Peek(int la = 1)
        {
            throw new NotSupportedException();
        }
    }
}
