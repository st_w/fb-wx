﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace interface_parser.Lexer
{
    interface ILexer
    {

        int Line { get; }
        int Column { get; }
        TokenPosition Position { get; }

        Token Next();
        Token Peek(int la = 1);

    }
}
