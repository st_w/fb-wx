﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using interface_parser.Lexer;

namespace interface_parser
{
    class Program
    {
        static void Main(string[] args)
        {

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();

            Stream s = File.OpenRead("string.h");
            Lexer_LA l = new Lexer_LA(new Lexer.Lexer_SimpleC(s));

            Token t;
            while ((t = l.Next()).Type != TokenType.Empty)
            {
                System.Diagnostics.Debug.WriteLine(t.ToString());
            }
            stopWatch.Stop();

            System.Diagnostics.Debug.WriteLine(stopWatch.ElapsedMilliseconds.ToString());

        }
    }
}
