﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using interface_parser.Lexer;

namespace interface_parser.Parser
{
    class ParserException : Exception
    {
        TokenPosition pos;

        public ParserException(TokenPosition pos = null, String message = null) : base( message )
        {
            this.pos = pos;
        }

        public override string ToString()
        {
            return (pos != null ? pos.ToString() : "") + Message ?? "";
            //return base.ToString();
        }
    }
}
