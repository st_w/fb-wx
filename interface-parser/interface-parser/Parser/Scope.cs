﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace interface_parser.Parser
{
    class Scope
    {

        Scope Parent { get; private set; }
        Obj Owner { get; private set; }

        Obj findObj(string id)
        {
            return findObjInternal(id) ?? ((Parent != null) ? Parent.findObj(id) : null);
        }

        Obj findObjInternal(string id)
        {
            return null;
        }

        Scope(Obj owner = null, Scope parent = null)
        {
            Owner = owner;
            Parent = parent;
        }

    }
}
