﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using interface_parser.Lexer;

namespace interface_parser.Parser
{
    class Parser
    {

        private ILexer lex;
        private Token tk;

        public Parser(ILexer lexer)
        {
            lex = lexer;
            next();
        }


        public void Parse()
        {

        }


        void Class()
        {
            check(TokenType.Class);
            string className = Identifier();
            NSBlock();
        }

        void NSBlock()
        {
            check(TokenType.LBrace);
            //TODO: declarations, classes, namespaces, enums, structs, typedefs, ...
            check(TokenType.RBrace);
        }


        void Block()
        {
            check(TokenType.LBrace);
            //TODO: statements, declarations
            check(TokenType.RBrace);
        }

        void Method()
        {
            MethodDecl();
            Block();
        }

        void MethodDecl()
        {
            Type();
            string methodName = Identifier();
            ParamList();
        }

        void Type()
        {
            //TODO!!
        }

        void ParamList()
        {
            check(TokenType.LPara);
            if (tk.Type != TokenType.RPara)
            {
                while (true)
                {
                    Type();
                    string paramName = Identifier();
                    if (tk.Type == TokenType.Equals)
                    {
                        next();
                        Expression();
                    }
                    if (tk.Type != TokenType.Comma) break;
                    check(TokenType.Comma);
                }
            }
            check(TokenType.RPara);
        }


        void Expression()
        {
            //TODO!!
        }




        string Identifier()
        {
            expect(TokenType.Identifier);
            string id = tk.StrValue;
            next();
            return id;
        }



        void TypeDef()
        {
            check(TokenType.Typedef);
            if (tk.Type == TokenType.Struct)
            {
                // TODO
            }
            else
            {
                Type();
                Identifier();
            }
            check(TokenType.Semicolon);
        }




        #region Internal Helper Methods

        void expect(TokenType t)
        {
            if (tk.Type != t) throw new ParserException(lex.Position, "Expected: " + t.ToString() + " (encountered: " + tk.ToString() + ")");
        }

        void expect(IEnumerable<TokenType> tokens)
        {
            if (!tk.Is(tokens)) throw new ParserException(lex.Position, "Expected: " + tokens.ToString() + " (encountered: " + tk.ToString() + ")");
        }



        Token check(TokenType t)
        {
            expect(t);
            Token t_ = tk;
            next();
            return tk;
        }

        Token check(IEnumerable<TokenType> tokens)
        {
            expect(tokens);
            Token t_ = tk;
            next();
            return tk;
        }

        Token next()
        {
            tk = lex.Next();
            return tk;
        }


        #endregion


    }
}
